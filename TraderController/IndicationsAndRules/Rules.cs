﻿using GlobalEntities.Enums;

namespace TraderController.IndicationsAndRules
{
    public class Rules
    {
        /// <summary>
        /// Check if we should buy or short given the current slope
        /// </summary>
        /// <param name="pastSlope"> Slope using the past point and past X - 1 timeframe </param>
        /// <param name="currentSlope"> Slope using currentpoint and past X timeframe</param>
        /// <param name="buyRule">   Double indicating at what slope value, we should buy </param>
        /// <param name="shortRule"> Double indicating at what slope value, we should short </param>
        /// <returns> State of the algorithm </returns>
        public BitcoinStates SlopeBuyRules(double pastSlope, double currentSlope, double buyRule, double shortRule)
        //public BitcoinStates SlopeBuyRules(double currentSlope, double buyRule)
        {
            //return (currentSlope > buyRule) ? BitcoinStates.buying : BitcoinStates.pending;
            
            // Only make the trade if we cross that point
            // Catches the case where the code is not running and the slope is positive
            return (pastSlope < buyRule && currentSlope > buyRule)
                ? BitcoinStates.buying
                : (pastSlope > shortRule && currentSlope < shortRule)
                    ? BitcoinStates.shorting // TODO once working, change to just selling and buying
                    : BitcoinStates.pending;
        }

        /// <summary>
        /// Check if we should sell our shares
        /// </summary>
        /// <param name="sellBoughtRule"> Double indicating at what slope value, we should sell our buys </param>
        /// <param name="sellShortRule"> Double indicating at what slope value, we should sell our shorts </param>
        /// <returns> State of the algorithm </returns>
        public BitcoinStates SlopeSellRules(double pastSlope, double currentSlope, double sellBoughtRule, double sellShortRule, BitcoinStates state)
        //public BitcoinStates SlopeSellRules(double currentSlope, double sellBoughtRule, BitcoinStates state)
        {
            //return (state == BitcoinStates.boughtBitcoin && currentSlope < sellBoughtRule)
            //        ? BitcoinStates.sellingBought: state;
            // old code, when we thought we could short
            //return (state == BitcoinStates.boughtBitcoin && currentSlope < sellBoughtRule)
            //        ? BitcoinStates.sellingBought
            //        : (state == BitcoinStates.shortedBitcoin && currentSlope > sellShortRule)
            //            ? BitcoinStates.sellingShort
            //            : state;

            return (state == BitcoinStates.boughtBitcoin && pastSlope > sellBoughtRule && currentSlope < sellBoughtRule)
                    ? BitcoinStates.sellingBought
                    : (state == BitcoinStates.shortedBitcoin && pastSlope < sellShortRule && currentSlope > sellShortRule)
                        ? BitcoinStates.sellingShort
                        : state;
        }

        /// <summary>
        /// Checks if we should buy or short according to the second derivative
        /// </summary>
        /// <param name="pastTradeConcavity"> Second Derivative of past trade </param>
        /// <param name="currentTradeConcavity"> Second Derivative of current trade </param>
        /// <returns> State of the algorithm </returns>
        //public BitcoinStates ConcavityBuyRules(double pastTradeConcavity, double currentTradeConcavity, BitcoinStates state)
        //{
        //    // Want to take action when hitting 0 (when the sign changes)
        //    return (pastTradeConcavity < 0 && currentTradeConcavity > 0)
        //            ? BitcoinStates.buying
        //            : (pastTradeConcavity > 0 && currentTradeConcavity < 0)
        //                ? BitcoinStates.shorting
        //                : BitcoinStates.pending;
        //}

        /// <summary>
        /// Checks if we should sell according to second derivative
        /// </summary>
        /// <param name="pastTradeConcavity"> Second Derivative of past trade </param>
        /// <param name="currentTradeConcavity"> Second Derivative of current trade </param>
        /// <returns> State of the algorithm </returns>
        //public BitcoinStates ConcavitySellRules(double pastTradeConcavity, double currentTradeConcavity, BitcoinStates state)
        //{
        //    // Want to take action when hitting 0 (when the sign changes)
        //    return (state == BitcoinStates.boughtBitcoin && pastTradeConcavity < 0 && currentTradeConcavity > 0)
        //            ? BitcoinStates.sellingBought
        //            : (state == BitcoinStates.sellingBought && pastTradeConcavity > 0 && currentTradeConcavity < 0)
        //                ? BitcoinStates.shorting
        //                : state;
        //}
    }
}

