﻿using System.Collections.Generic;
using System.Windows.Forms;
using HelpersController.Time;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.WindowsForms;

namespace GrapherController
{
    public partial class Grapher : Form
    {
        public Grapher()
        {
            // custom class in Grapher.Designer.cs
            InitializeComponent(); 
        }

        /// <summary>
        /// Graphs x, y data        
        /// </summary>
        /// <param name="dataPoints"> x and y points. </param>
        /// <param name="title"> Chart title </param>
        /// <param name="xLabel"> x axis title </param>
        /// <param name="yLabel"> y axis title </param>        
        // Need to pass in values here instead of the constructor because Program.cs is called and it cannot have the values I think
        //public void graphValues(List<DataPoint> moneyDataPoints, List<DataPoint> slopeDataPoints, List<DataPoint> bitcoinPrices,  string title, string xLabel, string yLabel, bool useDate)
        public void graphValues(List<DataPoint> moneyDataPoints, List<DataPoint> slopeDataPoints, string title, string xLabel, string yLabel, string yLabel2)
        {
            //Create Plotmodel object
            var myModel = new PlotModel { Title = title };
            //http://docs.oxyplot.org/en/latest/models/axes/DateTimeAxis.html
            myModel.Axes.Add(new DateTimeAxis
            {
                Position = AxisPosition.Bottom,

                //Minimum = DateTimeAxis.ToDouble(moneyDataPoints[0].X),
                //Maximum = DateTimeAxis.ToDouble(moneyDataPoints[moneyDataPoints.Count - 1].X),
                Minimum = moneyDataPoints[0].X,
                Maximum = moneyDataPoints[moneyDataPoints.Count - 1].X,                
                //Minimum = dataPoints[0].X,                
                //Maximum = dataPoints[dataPoints.Count - 1].X,

                MinorIntervalType = DateTimeIntervalType.Days,
                IntervalType = DateTimeIntervalType.Days,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,

                StringFormat = "yyyy-MM-dd ",
                //StringFormat = "dd/MM/yyyy",
                Title = xLabel
            });

            //var model = new PlotModel { Title = "DateTimeAxis" };

            //var startDate = DateTime.Now.AddDays(-10);
            //var endDate = DateTime.Now;

            //var minValue = DateTimeAxis.ToDouble(startDate);
            //var maxValue = DateTimeAxis.ToDouble(endDate);

            //model.Axes.Add(new DateTimeAxis { Position = AxisPosition.Bottom, Minimum = minValue, Maximum = maxValue, StringFormat = "M/d" });

            // Add axises
            //myModel.Axes.Add(new LinearAxis()
            //{
            //    Position = AxisPosition.Bottom,
            //    Title = xLabel
            //});

            myModel.Axes.Add(new LinearAxis()
            {
                Position = AxisPosition.Left,
                Key = yLabel,
                Title = yLabel
            });

            myModel.Series.Add(new LineSeries
            {
                ItemsSource = moneyDataPoints,
                YAxisKey = yLabel,
                Color = OxyColors.Red,
                Title = yLabel2
            });

            //myModel.Axes.Add(new LinearAxis()
            //{
            //    Position = AxisPosition.Right,
            //    Key = "Bitcoin Price",
            //    Title = "Bitcoin Price"
            //});

            //myModel.Series.Add(new LineSeries
            //{
            //    ItemsSource = bitcoinPrices,
            //    YAxisKey = "Bitcoin Price",
            //    Color = OxyColors.Green,
            //    Title = "BitcoinPrice"
            //});

            myModel.Axes.Add(new LinearAxis()
            {
                Position = AxisPosition.Right,
                Key = yLabel2,
                Title = yLabel2
            });
            
            myModel.Series.Add(new LineSeries
            {
                ItemsSource = slopeDataPoints,
                YAxisKey = yLabel2,
                Color = OxyColors.Blue,
                Title = yLabel2
            });

            //Create Plotview object
            var myPlot = new PlotView()
            {
                Model = myModel,
                Dock = DockStyle.Bottom,
                Location = new System.Drawing.Point(0, 0),
                Size = new System.Drawing.Size(300, 400),
                TabIndex = 0
            };

            //Add plot control to form
            Controls.Add(myPlot);
        }
    }
}
