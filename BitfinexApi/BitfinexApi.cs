﻿using System;
using System.Text;
using System.Security.Cryptography;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using GlobalEntities.Entities;
using GlobalEntities.Enums;

namespace BitfinexApi
{
    /// <summary>
    /// This class is the main control for the bitfinex api.
    /// </summary>
    public class BitfinexApiV1
    {
        private DateTime epoch = new DateTime(1970, 1, 1);

        private HMACSHA384 hashMaker; 
        private string Key;
        private int nonce = 0;
        private string Nonce
        {
            get
            {
                if (nonce == 0)
                {
                    nonce = (int)(DateTime.UtcNow - epoch).TotalSeconds;
                }
                return (nonce++).ToString();
            }
        }

        /// <summary>
        /// Create instance of a bitfinex api with the given permissions (dependant on the key/secret)
        /// </summary>
        /// <param name="key"> The user accounts api key. Determines which permissions the api instance with have. </param>
        /// <param name="secret"> Extra security from bitfinex, relates to the api key. </param>
        public BitfinexApiV1(string key, string secret)
        {
            hashMaker = new HMACSHA384(Encoding.UTF8.GetBytes(secret));
            this.Key = key;
        }

        /// <summary>
        /// The hex string. 
        /// </summary>
        /// <param name="bytes"> Bytes. </param>
        /// <returns> a string. </returns>
        private String GetHexString(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder(bytes.Length * 2);
            foreach (byte b in bytes)
            {
                sb.Append(String.Format("{0:x2}", b));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Gets the balances from the account.
        /// </summary>
        /// <returns> List of balances </returns>
        public BalancesResponse GetBalances()
        {
            BalancesRequest req = new BalancesRequest(Nonce);
            string response = SendRequest(req,"GET");
            return BalancesResponse.FromJSON(response); ;
        }

        /// <summary>
        /// Cancel all orders
        /// </summary>
        /// <returns> result = All orders cancelled or result = None to cancel </returns>
        public CancelOrderResponse CancelOrder(int order_id)
        {
            CancelOrderRequest req = new CancelOrderRequest(Nonce, order_id);
            string response = SendRequest(req,"POST");
            CancelOrderResponse resp = CancelOrderResponse.FromJSON(response);
            return resp;
        }
        public CancelAllOrdersResponse CancelAllOrders()
        {
            CancelAllOrdersRequest req = new CancelAllOrdersRequest(Nonce);
            string response = SendRequest(req,"GET");
            return CancelAllOrdersResponse.FromJSON(response);
        }
        public OrderStatusResponse GetOrderStatus(int order_id)
        {
            OrderStatusRequest req = new OrderStatusRequest(Nonce, order_id);
            string response = SendRequest(req, "POST");
            return OrderStatusResponse.FromJSON(response);
        }
        public ActiveOrdersResponse GetActiveOrders()
        {
            ActiveOrdersRequest req = new ActiveOrdersRequest(Nonce);
            string response = SendRequest(req, "POST");
            return ActiveOrdersResponse.FromJSON(response);
        }
        public ActivePositionsResponse GetActivePositions()
        {
            ActivePositionsRequest req = new ActivePositionsRequest(Nonce);
            string response = SendRequest(req, "POST");
            return ActivePositionsResponse.FromJSON(response);
        }

        public NewOrderResponse ExecuteBuyOrderBTC(decimal amount, decimal price, OrderExchange exchange, OrderType type)
        {
            return ExecuteOrder(OrderSymbol.BTCUSD, amount, price, exchange, OrderSide.Buy, type);
        }
        public NewOrderResponse ExecuteSellOrderBTC(decimal amount, decimal price, OrderExchange exchange, OrderType type)
        {
            return ExecuteOrder(OrderSymbol.BTCUSD, amount, price, exchange, OrderSide.Sell, type);
        }
        public NewOrderResponse ExecuteOrder(OrderSymbol symbol, decimal amount, decimal price, OrderExchange exchange, OrderSide side, OrderType type)
        {
            NewOrderRequest req = new NewOrderRequest(Nonce, symbol, amount, price, exchange, side, type);
            string response = SendRequest(req,"POST");
            NewOrderResponse resp = NewOrderResponse.FromJSON(response);
            return resp;
        }

        /// <summary>
        /// Gets trades.
        /// </summary>
        /// <param name="pastSeconds"> Get trades going back this many seconds. Default 60 seconds. </param>
        /// <param name="limit_trades"> Limit the number of trades returned. Must be >= 1 </param>
        /// <param name="currenciesTraded"> Specify the currencies of trades to return. Defaults to bitcoin and usd. </param>
        /// <returns> List of trades. </returns>
        //public TradeResponse GetTrades(double pastSeconds = 60, int limit_trades = 10, CurrencyPair currencyPair = null)        
        public TradeResponse GetTrades(double pastSeconds, int limit_trades, CurrencyPair currencyPair = null)
        {
            if (currencyPair == null)
            {
                currencyPair = new CurrencyPair { FirstCurrency = Currencies.btc, SecondCurrency = Currencies.usd };
            }

            string response;
            var req = new TradesRequest(pastSeconds, limit_trades, $"{currencyPair.ToString()}");
            try
            {
                response = SendRequest(req, "GET");
            }
            catch (WebException)
            {
                throw; // just throw the exception to caller
            }
            
            return TradeResponse.FromJSON(response, currencyPair);
        }

        /// <summary>
        /// Send a request to bitfinex.
        /// </summary>
        /// <param name="request"> The request to send. </param>
        /// <param name="httpMethod"> The method. </param>
        /// <returns> The response from the bitfinex server. </returns>        
        //private string SendRequest(NewOrderRequest request, string httpMethod, string a)
        private string SendRequest(GenericRequest request,string httpMethod)
        {
            string json = JsonConvert.SerializeObject(request);
            string json64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(json));
            byte[] data = Encoding.UTF8.GetBytes(json64);
            byte[] hash = hashMaker.ComputeHash(data);
            string signature = GetHexString(hash);

            HttpWebRequest wr = WebRequest.Create("https://api.bitfinex.com"+request.request) as HttpWebRequest;
            wr.Headers.Add("X-BFX-APIKEY", Key);
            wr.Headers.Add("X-BFX-PAYLOAD", json64);
            wr.Headers.Add("X-BFX-SIGNATURE", signature);
            wr.Method = httpMethod;
            
            string response = null;
            try
            {
                var resp = wr.GetResponse() as HttpWebResponse;
                var sr = new StreamReader(resp.GetResponseStream());
                response = sr.ReadToEnd();
                sr.Close();
            }
            // Occurs when not connected to the internet (testing with airplane mode   
            // Returns null value to TradeResponse which raises the error
            // System.Net.WebException: 'The remote name could not be resolved: 'api.bitfinex.com''
            catch (WebException)
            {
                //throw new BitfinexNotConnectedException(ex, "Probably not connected to the internet");
                throw;
            }
            return response;
        }
    }
}
