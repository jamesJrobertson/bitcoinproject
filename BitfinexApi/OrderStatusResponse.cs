﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BitfinexApi
{
    public class OrderStatusResponse
    {
        [Key]
        public Guid GuidId { get; set; }
        public string id { get; set; }
        public string symbol { get; set; }
        public string exchange { get; set; }
        public string price { get; set; }
        public string avg_execution_price { get; set; }
        public string side { get; set; }
        public string type { get; set; }
        public string timestamp { get; set; }
        public string is_live { get; set; }
        public string is_cancelled { get; set; }
        public string is_hidden { get; set; }
        public string was_forced { get; set; }
        public string executed_amount { get; set; }
        public string remaining_amount { get; set; }
        public string original_amount { get; set; }

        public static OrderStatusResponse FromJSON(string response)
        {            
            return JsonConvert.DeserializeObject<OrderStatusResponse>(response);
        }

        public override string ToString()
        {
            return $"id: {id}\n" +
            $"symbol: {symbol}\n" +
            $"exchange: {exchange}\n" +
            $"price: {price}\n" +
            $"avg_execution_price: {avg_execution_price}\n" +
            $"type: {type}\n" +
            $"timestamp: {timestamp}\n" +
            $"is_live: {is_live}\n" +
            $"is_cancelled: {is_cancelled}\n" +
            $"was_forced: {was_forced}\n" +
            $"executed_amount: {executed_amount}\n" +
            $"remaining_amount: {remaining_amount}\n" +
            $"original_amount: {original_amount}\n";
        }
    }
}
