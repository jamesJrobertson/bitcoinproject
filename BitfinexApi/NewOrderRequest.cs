﻿using System.Collections.Generic;
using System.Globalization;

namespace BitfinexApi
{
    public enum OrderType
    {

        //MarginMarket,
        //MarginLimit,
        //MarginStop,
        //MarginTrailingStop

        // A margin order in which a buy or sell order to be executed immediately at current market prices.
        market,

        // A margin order to buy/sell at a specified price or better.
        limit,

        // A margin order to sell or close your position once the market reaches a certain price.
        stop,

        // A margin stop order that can be set to execute once the market goes against you by a defined price,
        // called the price difference. Trailing–stop sell orders are used to maximize and protect profit 
        // as a stock's price rises and limit losses when its price falls.
        //[Description("trailing-stop")]
        trailingStop,

        // A margin order that is a limit order that must be filled in its entirety or canceled (killed).
        // The purpose of a fill or kill order is to ensure that a position is entered at a desired price.
        //[Description("fill-or-kill")]
        fillOrKill,

        // A exchange order in which a buy or sell order to be executed immediately at current market prices.
        //[Description("exchange market")]
        exchangeMarket,

        // A exchange order to buy/sell at a specified price or better.
        //[Description("exchange limit")]
        exchangeLimit,

        // A exchange order to sell or close your position once the market reaches a certain price.
        //[Description("exchange stop")]
        exchangeStop,

        // A exchange stop order that can be set to execute once the market goes against you by a defined price,
        // called the price difference. Trailing–stop sell orders are used to maximize and protect profit 
        // as a stock's price rises and limit losses when its price falls.
        //[Description("exchange trailing-stop")]
        exchangeTrailingStop,

        // A exchange order that is a limit order that must be filled in its entirety or canceled (killed).
        // The purpose of a fill or kill order is to ensure that a position is entered at a desired price.
        //[Description("exchange fill-or-kill")]
        exchangeFillOrKill

    }
    public enum OrderSide
    {
        Buy,
        Sell
    }
    public enum OrderExchange
    {
        Bitfinex,
        Bitstamp,
        All
    }
    public enum OrderSymbol
    {
        BTCUSD
    }

    public class NewOrderRequest : GenericRequest
    {
        public string symbol;
        public string amount;
        public string price;
        public string exchange;
        public string side;
        public string type;
        public bool is_hidden = false; // TODO write good comment here. not sure what this does

        public NewOrderRequest(string nonce, OrderSymbol symbol, decimal amount, decimal price, OrderExchange exchange, OrderSide side, OrderType type)
        {
            this.symbol = EnumHelper.EnumToStr(symbol);
            this.amount = amount.ToString(CultureInfo.InvariantCulture);
            this.price = price.ToString(CultureInfo.InvariantCulture);
            this.exchange = EnumHelper.EnumToStr(exchange);
            this.side = EnumHelper.EnumToStr(side);
            this.type = EnumHelper.EnumToStr(type);
            this.nonce = nonce;
            this.request = "/v1/order/new";
        }
    }
    public class EnumHelper
    {
        private static Dictionary<object, string> enumStr = null;
        private static Dictionary<object, string> Get()
        {
            if (enumStr == null)
            {
                enumStr = new Dictionary<object, string>();
                enumStr.Add(OrderSymbol.BTCUSD, "btcusd");

                enumStr.Add(OrderExchange.All, "all");
                enumStr.Add(OrderExchange.Bitfinex, "bitfinex");
                enumStr.Add(OrderExchange.Bitstamp, "bitstamp");

                enumStr.Add(OrderSide.Buy, "buy");
                enumStr.Add(OrderSide.Sell, "sell");

                enumStr.Add(OrderType.limit, "limit");
                enumStr.Add(OrderType.market, "market");
                enumStr.Add(OrderType.exchangeLimit, "exchange limit");
                enumStr.Add(OrderType.exchangeMarket, "exchange market");
                enumStr.Add(OrderType.stop, "stop");
                enumStr.Add(OrderType.trailingStop, "trailing-stop");
            }
            return enumStr;
        }

        public static string EnumToStr(object enumItem)
        {
            return Get()[enumItem];
        }
    }
}
