﻿using Newtonsoft.Json;

namespace BitfinexApi
{
    public class NewOrderResponse : OrderStatusResponse
    {
        public string order_id { get; set; }
        
        public static new NewOrderResponse FromJSON(string response)
        {
            NewOrderResponse resp = JsonConvert.DeserializeObject<NewOrderResponse>(response);
            return resp;
        }
    }
}
