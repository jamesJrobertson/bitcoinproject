﻿using AnalysisController.StatsCalculator;
using GlobalEntities.Entities;
using System.Collections.Generic;
using TraderController.IndicationsAndRules;
using OxyPlot;
using System.Linq;
using GlobalEntities.Enums;
using HelpersController.Time;

namespace AnalysisController.DataAnalysis
{
    public class PastDataAnalyzer
    {
        private const double TAKER_FEE = 0.002; // 0.2%
        private const double MAKER_FEE = 0.001; // 0.1%
        
        public (OutputVariables, List<double>, List<double>) AnalyzePastData(List<LongDataPoint> dataPoints, InputVariables inputVariables, double limitDataPercent, bool printToScreen)
        {
            var rules = new Rules();
            var totalPointsCount = dataPoints.Count;
            var slope = new CurveFitting.SlopeCalculator();
            // slightly faster to use an array, but then graphs needs to take in an array instead of a list (not sure if we wanna do that)            
                   
            // Initialize variables
            var graphPoints = new List<DataPoint>();

            // Loop through data and get slope over past secondLimits seconds
            var state = BitcoinStates.pending;
            var outputVariables = new OutputVariables();
            var money  = new List<double>(new double[totalPointsCount]);
            var slopes = new List<double>(new double[totalPointsCount]);
            money[0] = 0;
            var bitcoinOwned = 0.0;
            var pastSlope = 0.0;

            // Loop through data and get slope over past SecondLimits seconds            
            // Divide by 60 to convert from seconds to points because data is minute separated
            // Start at secondLimit because we don't want to start before we have enough data
            // (i.e., if taking average over past day, don't start until we have a day of data)

            for (var i =  1; i < totalPointsCount; i++)
            {                    
                // Don't do if statement to check if we have enough data because
                // code gets run in 10 hour loop (don't want to increase to 12 hours)
                (var limitedDataPoints, var haveEnoughData) = new LimitDataClass().LimitData(dataPoints, i, inputVariables.SecondLimit, limitDataPercent);

                if (haveEnoughData)
                { 
                    var currentSlope = slope.CalculateLinearRegression(limitedDataPoints);                    
                    slopes[i] = currentSlope;

                    // Assume if we haven't shorted or bought bitcoin then we are in a pending state.                
                    //state = (state == BitcoinStates.boughtBitcoin)
                        //? rules.SlopeSellRules(currentSlope, inputVariables.SellBoughtCurrencyLimit, state)
                        //: rules.SlopeBuyRules(currentSlope, inputVariables.BuyLimitRule);
                        // old code when we thought we could short
                    state = (state == BitcoinStates.shortedBitcoin || state == BitcoinStates.boughtBitcoin)
                        ? rules.SlopeSellRules(pastSlope, currentSlope, inputVariables.SellBoughtCurrencyLimit, inputVariables.SellShortCurrencyLimit, state)
                        : rules.SlopeBuyRules(pastSlope, currentSlope, inputVariables.BuyLimitRule, inputVariables.ShortLimitRule);

                    switch (state)
                    {
                        case BitcoinStates.buying:                            
                            state = BitcoinStates.boughtBitcoin;
                            bitcoinOwned += inputVariables.USDToTrade / dataPoints[i].YValue;
                            outputVariables.Trades.Buys += 1;
                            outputVariables.PercentLostToTrades += TAKER_FEE;
                            money[i] = money[i - 1] - TAKER_FEE * inputVariables.USDToTrade;

                            if (printToScreen)
                            {
                                System.Console.WriteLine("i: " + i + " : money " + money[i] + " : currentSlope: " + currentSlope + " : bitcoin price " + dataPoints[i].YValue);
                            }
                            
                            break;

                        // old code when we thought we could short
                        case BitcoinStates.shorting:
                            // Have negative bitcoin owned because we owe it
                            state = BitcoinStates.shortedBitcoin;
                            bitcoinOwned -= inputVariables.USDToTrade / dataPoints[i].YValue;
                            outputVariables.Trades.Shorts += 1;
                            outputVariables.PercentLostToTrades += TAKER_FEE;
                            money[i] = money[i - 1] - TAKER_FEE * inputVariables.USDToTrade;
                            if (printToScreen)
                            {
                                System.Console.WriteLine("i: " + i + " : money " + money[i] + " : currentSlope: " + currentSlope + " : bitcoin price " + dataPoints[i].YValue);
                            }
                            break;

                        case BitcoinStates.sellingBought:
                            // Need to subtract USDToTrade because did not do it when actually doing the trade
                            // Doing it this way makes the graph easier to read
                            money[i] = money[i - 1] - inputVariables.USDToTrade + bitcoinOwned * dataPoints[i].YValue - TAKER_FEE * inputVariables.USDToTrade;
                            bitcoinOwned = 0;
                            outputVariables.Trades.BuysSold += 1;
                            outputVariables.PercentLostToTrades += TAKER_FEE;
                            state = BitcoinStates.pending;
                            if (printToScreen)
                            {
                                System.Console.WriteLine("i: " + i + " : money " + money[i] + " : currentSlope: " + currentSlope + " : bitcoin price " + dataPoints[i].YValue);
                            }
                            break;

                        // old code when we thought we could short
                        case BitcoinStates.sellingShort:
                            // Keep in mind bitcoinOwned is negative here, USDToTrade is added instead of subtracted as well
                            money[i] = money[i - 1] + inputVariables.USDToTrade + bitcoinOwned * dataPoints[i].YValue - TAKER_FEE * inputVariables.USDToTrade;
                            bitcoinOwned = 0;
                            outputVariables.Trades.ShortsSold += 1;
                            outputVariables.PercentLostToTrades += TAKER_FEE;
                            state = BitcoinStates.pending;
                            if (printToScreen)
                            {
                                System.Console.WriteLine("i: " + i + " : money " + money[i] + " : currentSlope: " + currentSlope + " : bitcoin price " + dataPoints[i].YValue);
                            }
                            break;                                        

                        default:
                            // If no action is taken just write the previous totalUSD to the current totalUSD
                            money[i] = money[i - 1];
                            break;
                    }
                    pastSlope = currentSlope;
                }
                else
                {
                    money[i] = money[i - 1];
                }                
            }
            System.Console.WriteLine("money: " + money[money.Count - 1]);
            
            outputVariables.TimeSpan = dataPoints[dataPoints.Count - 1].XValue - dataPoints[0].XValue;
            outputVariables.FinalMoney = money.Last();
            outputVariables.MostMoney  = money.Max();
            outputVariables.LeastMoney = money.Min();

            return (outputVariables, money, slopes);
        }            
    }
}