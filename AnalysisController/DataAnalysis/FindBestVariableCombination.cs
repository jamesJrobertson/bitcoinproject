﻿using Context.BitcoinDatabase;
using GlobalEntities.Entities;
using GlobalEntities.Enums;
using Queries.InputVariablesEntity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace AnalysisController.DataAnalysis
{
    /// <summary>
    /// This class is used for analyzing the past data to find the best variable combinations to make the most profit.
    /// </summary>
    public class FindBestVariableCombination
    {
        BitcoinContext _context;
        double _limitDataPercent;

        public FindBestVariableCombination(BitcoinContext context, double limitDataPercent)
        {
            _context = context;
            _limitDataPercent = limitDataPercent;
        }

        /// <summary>
        /// Does a "mesh" of input variables to find the best set of them   
        /// by iteratively running pastDataAnalyzer
        /// </summary>
        /// <param name="dataPoints"> contains Unix Time stamp and Weighted USD of Bitcoin. </param>        
        public void LoopPastDataAnalyzer(List<LongDataPoint> dataPoints)
        {        
            InputVariables inputVariables;

            var stopwatch = Stopwatch.StartNew();
            var inputCombosAlreadyTried = new InputVariablesQueries(_context).GetInputVariableQuery();
            // 15d, 20d
            //var secondLimit = new int[] { (int) 1.296e6, (int) 1.728e6};
            //var secondLimit = new int[] {86400, 43200, 21600, 7200, 3600, 1800, 600, 300, 60, 864000, 172800};
            //1 minute - 2 days
            var secondLimit = new int[] {86400};

            // note, only checking symmetric options right now which we don't have to 
            // (i.e., short and buy are the same values) 
            // 5 buy and sell at 2 was best for 60 second time frame            
            //var buyRule       = new double[] {5e-3, 1e-3, 5e-4, 1e-4, 5e-5};
            //var sellRule      = new double[] {5e-3, 1e-3, 5e-4, 1e-4, 5e-5};
            //var shortRule     = new double[] {5e-3, 1e-3, 5e-4, 1e-4, 5e-5 };
            //var sellShortRule = new double[] {5e-3, 1e-3, 5e-4, 1e-4, 5e-5 };
            var buyRule = new double[] { 10e-4, 5e-4, 1e-4, 5e-5, 1e-5 };
            var sellRule = new double[] { 10e-4, 5e-4, 1e-4, 5e-5, 1e-5 };
            var shortRule = new double[] { 10e-4, 5e-4, 1e-4, 5e-5, 1e-5 };
            var sellShortRule = new double[] { 10e-4, 5e-4, 1e-4, 5e-5, 1e-5 };
            var counter = 0.0;

            for (var a = 0; a < secondLimit.Length; a++)
            {
                for (var b = 0; b < buyRule.Length; b++)
                {
                    for (var c = 0; c < sellRule.Length; c++)
                    {
                        for (var d = 0; d < shortRule.Length; d++)
                        {
                            for (var e = 0; e < sellShortRule.Length; e++)
                            {
                                counter++;
                                Console.WriteLine(counter / (secondLimit.Length * buyRule.Length * sellRule.Length * shortRule.Length * sellShortRule.Length) * 100 + "% done");

                                inputVariables = new InputVariables
                                {
                                    SecondLimit = secondLimit[a],
                                    BuyLimitRule = buyRule[b],
                                    SellBoughtCurrencyLimit = sellRule[c],
                                    ShortLimitRule = -shortRule[d],                                    
                                    SellShortCurrencyLimit = -sellShortRule[e],
                                    USDToTrade = 100, // use 100 because return is same as % then
                                    Method = Algorithm.poly1
                                };

                                Console.WriteLine("\nCurrent Combo trying");
                                Console.WriteLine(inputVariables.ToString());

                                // Skip if these criterias are met or we have already tried this combination                                
                                if (SkipCurrentCombination(secondLimit[a], buyRule[b], sellRule[c], -shortRule[d], -sellShortRule[e]) || inputCombosAlreadyTried.Any(icat => icat.Equals(inputVariables)))
                                {
                                    continue;
                                }

                                (var outputVariables, var money, var slopes) = new PastDataAnalyzer().AnalyzePastData(dataPoints, inputVariables, _limitDataPercent, false);
                                outputVariables.InputVariables = inputVariables;
                                Console.WriteLine("final money:" + outputVariables.FinalMoney);

                                WriteInputsAndOutputsToTables(inputVariables, outputVariables);
                            }
                        }
                    }
                }
            }
            // Print total time to screen out of curiosity
            stopwatch.Stop();
            Console.WriteLine("Total runtime: " + stopwatch.ElapsedMilliseconds / 1000 + "s");
        }

        /// <summary>
        /// Function to check if we should be skipping the current combination.
        /// </summary>
        /// <param name="secondLimit"> The seconds limit. </param>
        /// <param name="buyRule"> The buy rule. </param>
        /// <returns> True if the combination should be skipped. </returns>
        private bool SkipCurrentCombination(int secondLimit, double buyRule, double sellRule, double shortRule, double sellShortRule)
        {
            return (secondLimit >= 43200 && buyRule >= 0.1) ||
                   (secondLimit >= 21600 && buyRule > 0.1) ||
                   (secondLimit >= 10800 && buyRule > 0.3) ||
                   (secondLimit >= 10800 && buyRule > 0.3) ||
                   (secondLimit >= 3600 && buyRule > 0.5) ||
                   (secondLimit >= 1800 && buyRule >= 2) ||
                   (secondLimit >= 600 && buyRule >= 5) ||
                   (secondLimit >= 300 && buyRule >= 8);
        }

        private void WriteInputsAndOutputsToTables(InputVariables inputVariables, OutputVariables outputVariables)
        {
            _context.InputVariables.Add(inputVariables);
            _context.OutputVariables.Add(outputVariables);
            _context.SaveChanges();
        }       
    }   
}
