﻿using AnalysisController.StatsCalculator;
using BitfinexApi;
using Context.BitcoinDatabase;
using GlobalEntities.Entities;
using GlobalEntities.Enums;
using HelpersController.Time;
using Queries.BitcoinAveragesEntity;
using Queries.StateEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using TraderController.IndicationsAndRules;

namespace AnalysisController.DataAnalysis
{
    public class PresentDataAnalyzer
    {
        /// <summary>
        /// Buys and Sells Bitcoin Depending on the Market
        /// </summary>
        /// <param name="_context">  The database </param>
        /// <param name="inputVariables"> Slope settings, seconds to average over </param>
        /// <param name="_writeApi"> Write API </param>        
        public (double, double) BuySellBitcoinOrDoNothing(BitcoinContext _context, BitfinexApiV1 _writeApi, BitfinexApiV1 _readApi,
             InputVariables inputVariables, decimal randomPriceToSetForMarketTrading, double limitDataPercent, double pastBuySlope, double pastSellSlope)
        {
            Console.WriteLine($"BuySellBitcoinOrDoNothing() called at {DateTime.Now.ToLongTimeString()}\n");

            (var pastBuyDataPoints, var pastSellDataPoints) = GetOurData(_context);

            // Get current state of the algorithm            
            var currentState = new StateQueries(_context).GetState();

            // initialie variable
            BitcoinStates newState = BitcoinStates.pending;

            // Get the new state, with the most recent data
            (newState, pastBuySlope, pastSellSlope) = GetNewState(pastBuyDataPoints, pastSellDataPoints, inputVariables, currentState, limitDataPercent, pastBuySlope, pastSellSlope);
            
            switch (newState)
            {
                case (BitcoinStates.sellingShort):
                case (BitcoinStates.buying):
                    {                        
                        // calculate the amount of bitcoin to trade
                        // TODO think this isn't 100% accurate. Should get what the current market price is. 
                        // TODO could just be done better. Create function or set class variable or set inputVariables.
                        // TODO change inputVariables to have amount to trade
                        var buyBitcoinToTrade = (decimal) inputVariables.USDToTrade / pastBuyDataPoints[pastBuyDataPoints.Count - 1].YValue;

                        // Use BitfinexApi.OrderType.exchangeLimit to set your own price
                        var buyOrderResponse = _writeApi.ExecuteOrder(OrderSymbol.BTCUSD, buyBitcoinToTrade, randomPriceToSetForMarketTrading,
                        OrderExchange.Bitfinex, OrderSide.Buy, BitfinexApi.OrderType.exchangeMarket);                        
                        buyOrderResponse.GuidId = Guid.NewGuid(); // TODO should be done in object creation           

                        newState = (newState == BitcoinStates.buying) ? BitcoinStates.boughtBitcoin : BitcoinStates.pending;                        

                        //buyOrderOrderResponse.ToString(); // TODO

                        _context.NewOrderResponses.Add(buyOrderResponse);
                        _context.SaveChanges();
                        break;
                    }

                // Place order to sell
                case (BitcoinStates.shorting):
                case (BitcoinStates.sellingBought):

                    // create variables for clarity
                    var balanceResponse = _readApi.GetBalances();
                    var sellBitcoinToTrade = (decimal) balanceResponse.AllBalances[0].AvailableAmount;

                    var sellOrderResponse = _writeApi.ExecuteOrder(OrderSymbol.BTCUSD, sellBitcoinToTrade, randomPriceToSetForMarketTrading,
                    OrderExchange.Bitfinex, OrderSide.Sell, BitfinexApi.OrderType.exchangeMarket);                    
                    sellOrderResponse.GuidId = Guid.NewGuid();

                    //sellOrderResponse.ToString(); // TODO
                    newState = (newState == BitcoinStates.sellingBought) ? BitcoinStates.pending : BitcoinStates.shortedBitcoin;

                    _context.NewOrderResponses.Add(sellOrderResponse);
                    _context.SaveChanges();
                    break;

                // Do not include pending state. Just go to default because has the same functionality
                // case (BitcoinStates.pending):
                default:
                    break;
                
            }
            Console.WriteLine(pastBuySlope);
            return (pastBuySlope, pastSellSlope);
        }

        public (List<LongDataPoint> pastBuyDataPoints, List<LongDataPoint> pastSellDataPoints) GetOurData(BitcoinContext _context)
        {
            // get all averages in averages table               
            var pastPrices = new BitcoinAveragesQueries(_context).GetBitcoinAveragesOrderByTimeStampUtc().ToList();

            var pastBuyDataPoints = new List<LongDataPoint>();
            var pastSellDataPoints = new List<LongDataPoint>();

            foreach (var pastPrice in pastPrices)
            {
                var dataPoint = new LongDataPoint()
                {
                    XValue = (long)TimeConversions.UtcDateTimeToUnixSeconds(pastPrice.TimeStampUtc),
                    YValue = (long)pastPrice.AveragePrice
                };

                if (pastPrice.IsBuy == true) pastBuyDataPoints.Add(dataPoint);
                else pastSellDataPoints.Add(dataPoint);
            }

            return (pastBuyDataPoints, pastSellDataPoints);
        }

        /// <summary>
        /// Given the past buy and sell data, input variables, and current state, return the new state
        /// </summary>
        /// <param name="pastBuyDataPoints">  List of past buys on bitfinex api </param>
        /// <param name="pastSellDataPoints"> List of past sells on bitfinex api </param>
        /// /// <param name="inputVariables"> Slope settings, seconds to average over </param>
        /// /// <param name="state"> Current state of the algorithm </param>
        /// // TODO get new state could have a test
        public (BitcoinStates, double, double) GetNewState(List<LongDataPoint> pastBuyDataPoints, List<LongDataPoint> pastSellDataPoints, InputVariables inputVariables, BitcoinStates state, double limitDataPercent, double pastBuySlope, double pastSellSlope)
        {               
            var rules = new Rules();
            var slope = new CurveFitting.SlopeCalculator();            

            (var limitedBuyDataPoints, var haveEnoughBuyData) = new LimitDataClass().LimitData(pastBuyDataPoints, pastBuyDataPoints.Count - 1, inputVariables.SecondLimit, limitDataPercent);
            (var limitedSellDataPoints, var haveEnoughSellData) = new LimitDataClass().LimitData(pastSellDataPoints, pastSellDataPoints.Count - 1, inputVariables.SecondLimit, limitDataPercent);

            // Set slope to 0 if we don't have enough points
            var currentBuySlope = (haveEnoughBuyData) ? slope.CalculateLinearRegression(limitedBuyDataPoints) : 0;
            var currentSellSlope = (haveEnoughSellData) ? slope.CalculateLinearRegression(limitedSellDataPoints): 0;

            // If it is the first iteration fo the code and pastBuySlope hasn't been created yet
            if (pastBuySlope == default(double))
                return (BitcoinStates.pending, currentBuySlope, currentSellSlope);

            Console.WriteLine("current buy slope:  " + currentBuySlope + "\n");
            Console.WriteLine("current sell slope: " + currentSellSlope + "\n");
            // Assume if we haven't shorted or bought bitcoin then we are in a pending state.
            //var newBitcoinState = (state == BitcoinStates.boughtBitcoin)
            //        ? rules.SlopeSellRules(currentSellSlope, inputVariables.SellBoughtCurrencyLimit, state)
            //        : rules.SlopeBuyRules(currentBuySlope, inputVariables.BuyLimitRule);
            var newBitcoinState = (state == BitcoinStates.shortedBitcoin || state == BitcoinStates.boughtBitcoin)
                        ? rules.SlopeSellRules(pastSellSlope, currentSellSlope, inputVariables.SellBoughtCurrencyLimit, inputVariables.SellShortCurrencyLimit, state)
                        : rules.SlopeBuyRules(pastBuySlope, currentBuySlope, inputVariables.BuyLimitRule, inputVariables.ShortLimitRule);

            return (newBitcoinState, currentBuySlope, currentSellSlope);
        }
    }
}