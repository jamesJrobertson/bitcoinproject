﻿using System;
using System.Collections.Generic;
using GlobalEntities.Entities;
using Context.Sql;
using Context.BitcoinDatabase;

namespace AnalysisController.HistoricalDataReader
{
    public class PastAveragesManager
    {
        BitcoinContext _context;

        public PastAveragesManager(BitcoinContext context)
        {
            _context = context;
        }
        
        public void RecreateAveragesTable()
        {
            _context.Database.ExecuteSqlCommand("TRUNCATE TABLE [Averages]");
            var lines = System.IO.File.ReadAllLines(@"..\..\..\PastDataFiles\pastAverages.txt");            

            var pastAverageList = new List<Average>();

            // Start at 1 to skip header
            for (var i = lines.Length - 1; i > 0; --i)
            {
                var str = lines[i].Split('\t');

                var pastAverage = new Average(){
                    Id = Guid.Parse(str[0]),
                    AveragePrice = double.Parse(str[1]),
                    IsBuy = (str[2] == "1"),
                    TotalAmount = double.Parse(str[3]),
                    TotalCount = double.Parse(str[4]),
                    TimeStampUtc = DateTime.Parse(str[5]),
                    TimeStampLocal = DateTime.Parse(str[6])
                };            
                pastAverageList.Add(pastAverage);                
            }

            Console.WriteLine("Writing to averages table");
            _context.Averages.AddRange(pastAverageList);
            _context.SaveChanges();
            Console.WriteLine("Finished adding past averages to averages table");
        }        
    }
}
