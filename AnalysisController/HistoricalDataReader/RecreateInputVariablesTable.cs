﻿using System;
using System.Collections.Generic;
using GlobalEntities.Entities;
using GlobalEntities.Enums;
using Context.BitcoinDatabase;

namespace AnalysisController.HistoricalDataReader
{
    public class PastInputVariablesManager
    {
        BitcoinContext _context;

        public PastInputVariablesManager(BitcoinContext context)
        {
            _context = context;
        }

        public void RecreateInputVariablesTable()
        {            
            var lines = System.IO.File.ReadAllLines(@"..\..\..\..\PastDataFiles\pastInputVariables.txt");

            var pastInputVariablesList = new List<InputVariables>();

            // Start at 1 to skip header
            for (var i = lines.Length - 1; i > 0; --i)
            {
                var str = lines[i].Split('\t');

                var pastInputVariables = new InputVariables() {
                    Id = Guid.Parse(str[0]),
                    SecondLimit = int.Parse(str[1]),
                    BuyLimitRule = double.Parse(str[2]),
                    SellBoughtCurrencyLimit = double.Parse(str[3]),
                    ShortLimitRule = double.Parse(str[4]),
                    SellShortCurrencyLimit = double.Parse(str[5]),
                    USDToTrade = double.Parse(str[6]),
                    Method = Algorithm.poly1, // Note: just hardcoded this
                    TimeStampUtc = DateTime.Parse(str[8]),
                    TimeStampLocal = DateTime.Parse(str[9])
                    };

                pastInputVariablesList.Add(pastInputVariables);
            }

            Console.WriteLine("Writing to input variables table");
            _context.InputVariables.AddRange(pastInputVariablesList);
            _context.SaveChanges();
            Console.WriteLine("Finished adding past input variables to input variables table");
        }
    }
}
