﻿using System;
using System.Collections.Generic;
using GlobalEntities.Entities;
using Context.Sql;
using Context.BitcoinDatabase;

namespace AnalysisController.HistoricalDataReader
{
    public class PastOutputVariablesManager
    {
        BitcoinContext _context;

        public PastOutputVariablesManager(BitcoinContext context)
        {
            _context = context;
        }

        public void RecreateOutputVariablesTable()
        {
            var lines = System.IO.File.ReadAllLines(@"..\..\..\..\PastDataFiles\pastOutputVariables.txt");

            var pastOutputVariablesList = new List<OutputVariables>();

            // Start at 1 to skip header
            for (var i = lines.Length - 1; i > 0; --i)
            {
                var str = lines[i].Split('\t');

                // Need to initialize first to construct trades part
                var pastOutputVariables = new OutputVariables();                
                pastOutputVariables.Id = Guid.Parse(str[0]);
                pastOutputVariables.Trades.Buys= int.Parse(str[1]);
                pastOutputVariables.Trades.BuysSold = int.Parse(str[2]);
                pastOutputVariables.Trades.Shorts = int.Parse(str[3]);
                pastOutputVariables.Trades.ShortsSold = int.Parse(str[4]);
                pastOutputVariables.PercentLostToTrades = double.Parse(str[5]);
                pastOutputVariables.TimeSpan= long.Parse(str[6]);                    
                pastOutputVariables.TimeStampUtc = DateTime.Parse(str[7]);
                pastOutputVariables.TimeStampLocal = DateTime.Parse(str[8]);
                pastOutputVariables.FinalMoney = double.Parse(str[9]);
                pastOutputVariables.MostMoney = double.Parse(str[10]);
                pastOutputVariables.LeastMoney = double.Parse(str[11]);
                pastOutputVariables.InputVariablesId = Guid.Parse(str[12]);

                pastOutputVariablesList.Add(pastOutputVariables);
            }

            Console.WriteLine("Writing to output variables table");
            _context.OutputVariables.AddRange(pastOutputVariablesList);
            _context.SaveChanges();
            Console.WriteLine("Finished adding past output variables to output variables table");
        }
    }
}
