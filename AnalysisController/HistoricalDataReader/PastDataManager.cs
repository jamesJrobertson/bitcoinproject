﻿using System;
using System.Collections.Generic;
using GlobalEntities.Entities;
using GlobalEntities.Enums;
using Context.BitcoinDatabase;

namespace AnalysisController.HistoricalDataReader
{
    public class PastDataManager
    {
        BitcoinContext _context;

        public PastDataManager(BitcoinContext context)
        {
            _context = context;
        }

        // Notes:
        //  - AddRange runs out of memory when used on entire bitcoin history
        // Improvements:         
        //  - Does not create the table if it doesn't exist
        //  - Table needs to exist before code runs
        public void CreatePastDatabase()
        {
            _context.Database.ExecuteSqlCommand("TRUNCATE TABLE [PastDatas]");
            var lines = System.IO.File.ReadAllLines(@"..\..\..\PastDataFiles\coinbaseUSD_edited_last_3_months.csv");

            var pastDataList = new List<PastData>();

            // Start at 1 to skip header
            for (var i = lines.Length - 1; i > 0; --i)
            {
                var currencyPair = new CurrencyPair()
                {
                    FirstCurrency = Currencies.btc,
                    SecondCurrency = Currencies.usd
                };

                var str = lines[i].Split(',');
                var PastDataVar = new PastData()
                {
                    TimeStampUnix = Int32.Parse(str[1]),
                    TimeStampUtc = DateTime.Parse(str[2]),
                    TimeStampLocal = DateTime.Parse(str[3]),
                    TotalAmount = float.Parse(str[4]),
                    TotalUSD = float.Parse(str[5]),
                    WeightedUSD = float.Parse(str[6]),
                    CurrencyPair = currencyPair
                };                
                
                pastDataList.Add(PastDataVar);                
            }

            Console.WriteLine("Writing to database");
            _context.PastData.AddRange(pastDataList);
            _context.SaveChanges();
            Console.WriteLine("Finished adding data to past database");
        }     
    }
}
