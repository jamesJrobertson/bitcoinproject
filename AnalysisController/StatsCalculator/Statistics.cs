﻿using System.Collections.Generic;
using GlobalEntities.Entities;
using GlobalEntities.Enums;
using BitfinexApi;
using System;

namespace AnalysisController.StatsCalculator
{
    public class Statistics
    {
        private const double USD_CAD_EXCHANGE_RATE = 1.22;
        private const double SLOPE_THRESHOLD = 5;

        /// <summary>
        /// Calculates the total value of the account in us dollars.
        /// A recent trade is used to calculate the value of the cryptocurrencies.
        /// </summary>
        /// <param name="balancesResponse"> All the balances from Bitfinex. </param>
        /// <param name="tradeResponseItem"> Current price. </param>
        /// <returns> Total value of all currencies in USD. </returns>
        public double TotalValueUSD(BalancesResponse balancesResponse, TradeResponseItem tradeResponseItem)
        {
            //var USD = balancesResponse.totalAvailableUSD;
            //var BTCUSD = tradeResponseItem.price * balancesResponse.totalBTC;
            //return USD + BTCUSD;
            return 0.0;
        }

        /// <summary>
        /// Calculates the total value of the account in canadian dollars.
        /// A recent trade is used to calculate the value of the cryptocurrencies.
        /// </summary>
        /// <param name="balancesResponse"> All the balances from Bitfinex. </param>
        /// <param name="tradeResponseItem"> Current price. </param>
        /// <returns> Total value of all currencies in CAD. </returns>
        public double TotalValueCAD(BalancesResponse balancesResponse, TradeResponseItem tradeResponseItem)
        {
            return TotalValueUSD(balancesResponse, tradeResponseItem) * USD_CAD_EXCHANGE_RATE;
        }

        /// <summary>
        /// Calculates the average of the given <see cref="TradeResponseItem"/>.
        /// </summary>
        /// <param name="tradeResponseItems"> The trade data from the API. </param>
        /// <returns> Average price of all the trade items. </returns>
        public double CalculateAveragePrice(List<TradeResponseItem> tradeResponseItems)
        {
            double average = 0.0;
            tradeResponseItems.ForEach(t => average += t.price);
            return average / tradeResponseItems.Count;
        }
        
        public List<Average> CreateAverages(TradeResponse tradeResponse)
        {
            var sellAverage = new Average() { IsBuy = false };
            var buyAverage = new Average() { IsBuy = true };

            foreach (var trade in tradeResponse._trades)
            {
                if(trade.TradeType == TradeType.sell)
                {
                    sellAverage.AveragePrice += trade.Price;
                    sellAverage.TotalAmount += trade.Amount;
                    sellAverage.TotalCount++;
                    sellAverage.Trades.Add(trade);
                }

                if (trade.TradeType == TradeType.buy)
                {
                    buyAverage.AveragePrice += trade.Price;
                    buyAverage.TotalAmount += trade.Amount;
                    buyAverage.TotalCount++;
                    buyAverage.Trades.Add(trade); // we don't use this but leave it just in case we do later
                }
            }

            buyAverage.AveragePrice = buyAverage.AveragePrice / buyAverage.TotalCount;
            sellAverage.AveragePrice = sellAverage.AveragePrice / sellAverage.TotalCount;

            return new List<Average> { sellAverage, buyAverage};
        }

        /// <summary>
        /// Prints out the average price and sets it to the past average if there were no trades in the past X timeframe
        /// </summary>
        /// <param name="averages"> List containing average buy and sell price. </param>
        /// <param name="pastMinuteAverage"> List containing past minute average buy and sell price. </param>
        /// <returns> past average over X timeframe </returns>
        public (List<Average>, List<Average>) fixAveragesIfMissingData(List<Average> averages, List<Average> pastMinuteAverage)
        {            
            // Catch case where there were no sells that minute
            if (averages[0].TotalCount != 0)
            {
                Console.WriteLine($"Average sell price for past minute:\n" + $"{averages[0].ToString()}\n");
                pastMinuteAverage[0] = averages[0];
            }

            else if (pastMinuteAverage[0] != null)
            {
                Console.WriteLine("No sell points in past minute");
                averages[0].AveragePrice = pastMinuteAverage[0].AveragePrice;
                averages[0].TotalAmount = 0;
                averages[0].TotalCount = 0;
            }

            else
            {
                Console.WriteLine("No sell points and code just started");
            }

            // Catch case where there were no buys that minute
            if (averages[1].TotalCount != 0)
            {
                Console.WriteLine($"Average buy price for past minute:\n" + $"{averages[1].ToString()}");
                pastMinuteAverage[1] = averages[1];
            }

            else if (pastMinuteAverage[1] != null)
            {
                Console.WriteLine("No buy points in past minute");
                averages[1].AveragePrice = pastMinuteAverage[1].AveragePrice;
                averages[1].TotalAmount = 0;
                averages[1].TotalCount = 0;
            }

            else
            {
                Console.WriteLine("No buy points and code just started");
            }

            return (averages, pastMinuteAverage);
        }
    }
}
