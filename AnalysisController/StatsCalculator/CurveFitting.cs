﻿using System;
using System.Collections.Generic;
using GlobalEntities.Entities;
using System.Linq;

namespace AnalysisController.StatsCalculator
{
    public class CurveFitting
    {
        // modified from https://stackoverflow.com/questions/43224/how-do-i-calculate-a-trendline-for-a-graph
        // Checked that results are correct by hand and using Python
        public class SlopeCalculator
        {
            public double CalculateLinearRegression(IEnumerable<LongDataPoint> dataPoints)
            {                
                var trendy = new Trendline(dataPoints);
                return trendy.Slope;
            }
        }

        private class Trendline
        {
            private int Count;
            private long XAxisValuesSum; // changed to Int64 to prevent overflow
            private double YAxisValuesSum;
            private long XXSum = 0;
            private double XYSum = 0;

            public double Slope { get; private set; }

            public Trendline(IEnumerable<LongDataPoint> dataPoints)
            {
                Count = dataPoints.Count();

                foreach (var dataPoint in dataPoints)
                {
                    YAxisValuesSum += dataPoint.YValue;
                    XAxisValuesSum += dataPoint.XValue;

                    XYSum += (dataPoint.XValue * dataPoint.YValue);
                    XXSum += (dataPoint.XValue * dataPoint.XValue);
                }

                Slope = CalculateSlope();
            }

            private double CalculateSlope()
            {
                try
                {
                    // Linear Regression formula
                    return ((Count * XYSum) - (XAxisValuesSum * YAxisValuesSum)) / ((Count * XXSum) - (XAxisValuesSum * XAxisValuesSum));
                }
                catch (DivideByZeroException)
                {
                    return 0;
                }
            }
        }
    }
}
