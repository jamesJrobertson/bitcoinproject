﻿using System.Net;

namespace ApiController.ApiV1
{
    class BitfinexException : WebException
    {
        public BitfinexException(WebException ex, string bitfinexMessage) :
            base(bitfinexMessage, ex) {}
    }
}
