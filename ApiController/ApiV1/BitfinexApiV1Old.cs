﻿using System;
using ApiController.ApiV1.Responses;
using HelpersController.Logger;
using System.Text;
using System.Security.Cryptography;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using GlobalEntities.Enums;
using GlobalEntities.Entities;
using System.Threading;
using ApiController.ApiV1.Requests;

namespace ApiController.ApiV1
{
    /// <summary>
    /// This class is the main control for the bitfinex api.
    /// </summary>
    public class BitfinexApiV1Old
    {
        private HMACSHA384 _hashMaker;
        private string _key;
        
        /// <summary>
        /// Create instance of a bitfinex api with the given permissions (dependant on the key/secret)
        /// </summary>
        /// <param name="key"> The user accounts api key. Determines which permissions the api instance with have. </param>
        /// <param name="secret"> Extra security from bitfinex, relates to the api key. </param>
        public BitfinexApiV1Old(string key, string secret)
        {
            _hashMaker = new HMACSHA384(Encoding.UTF8.GetBytes(secret));
            _key = key;
        }

        /// <summary>
        /// The hex string. 
        /// </summary>
        /// <param name="bytes"> Bytes. </param>
        /// <returns> a string. </returns>
        private string GetHexString(byte[] bytes)
        {
            var sb = new StringBuilder(bytes.Length * 2);
            foreach (byte b in bytes)
            {
                sb.Append(String.Format("{0:x2}", b));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Send a request to bitfinex.
        /// </summary>
        /// <param name="request"> The request to send. </param>
        /// <param name="httpMethod"> The method. </param>
        /// <returns> The response from the bitfinex server. </returns>        
        //private string SendRequest(NewOrderRequest request, string httpMethod, string a)
       private string SendRequest(GenericRequest request, string httpMethod)
        {
            var json = JsonConvert.SerializeObject(request);
            var json64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(json));
            var data = Encoding.UTF8.GetBytes(json64);
            var hash = _hashMaker.ComputeHash(data);
            var signature = GetHexString(hash);            

            var wr = WebRequest.Create("https://api.bitfinex.com" + request.request) as HttpWebRequest;
            wr.Headers.Add("X-BFX-APIKEY", _key);
            wr.Headers.Add("X-BFX-PAYLOAD", json64);
            wr.Headers.Add("X-BFX-SIGNATURE", signature);
            wr.Method = httpMethod;

            string response = null;
            
            try
            {
                var resp = wr.GetResponse() as HttpWebResponse;
                var sr = new StreamReader(resp.GetResponseStream());
                response = sr.ReadToEnd();
                sr.Close();
            }
            // Occurs when not connected to the internet (testing with airplane mode   
            // Returns null value to TradeResponse which raises the error
            // System.Net.WebException: 'The remote name could not be resolved: 'api.bitfinex.com''
            catch (WebException)
            {
                //throw new BitfinexNotConnectedException(ex, "Probably not connected to the internet");
                throw;
            }
            return response;
        }

        /// <summary>
        /// Gets the balances from the account.
        /// </summary>
        /// <returns> List of balances </returns>
        public BalanceResponseOld GetBalances()
        {
            var req = new BalancesRequest();
            var response = SendRequest(req, "GET");
            return BalanceResponseOld.FromJSON(response);
        }

        /// <summary>
        /// Gets trades.
        /// </summary>
        /// <param name="pastSeconds"> Get trades going back this many seconds. Default 60 seconds. </param>
        /// <param name="limit_trades"> Limit the number of trades returned. Must be >= 1 </param>
        /// <param name="currenciesTraded"> Specify the currencies of trades to return. Defaults to bitcoin and usd. </param>
        /// <returns> List of trades. </returns>
        public TradeResponse GetTrades(double pastSeconds = 60, int limit_trades = 10, CurrencyPair currencyPair = null)
        {
            if (currencyPair == null)
            {
                currencyPair = new CurrencyPair { FirstCurrency = Currencies.btc, SecondCurrency = Currencies.usd };
            }
            string response;
            var req = new TradesRequest(pastSeconds, limit_trades, $"{currencyPair.ToString()}");
            try
            {
                response = SendRequest(req, "GET");
            }
            catch (WebException)
            {                
                throw; // just throw the exception to caller
            }
            
            return TradeResponse.FromJSON(response, currencyPair);
        }

        public NewOrderResponse NewOrder(string symbol, double amount, double price, string side, string type)
        {
            NewOrderRequest req = new NewOrderRequest(symbol, amount, price, side, type);
            var response = SendRequest(req, "POST");
            return NewOrderResponse.FromJSON(response);
        }

        /// <summary>
        /// Cancel all orders
        /// </summary>
        /// <returns> result = All orders cancelled or result = None to cancel </returns>
        public CancelAllOrdersResponse CancelAllOrders()
        {
            var req = new CancelAllOrdersRequest();
            var response = SendRequest(req, "POST");            
            return CancelAllOrdersResponse.FromJSON(response);
        }
    }
}