﻿namespace ApiController.ApiV1.Requests
{
    class CancelAllOrdersRequest : GenericRequest
    {
        /// <summary>
        /// Cancel all orders
        /// </summary>
        public CancelAllOrdersRequest()
        {
            this.request = "/v1/order/cancel/all";
        }
    }
}
