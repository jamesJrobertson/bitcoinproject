﻿using HelpersController.Time;

namespace ApiController.ApiV1 
{
    class TradesRequest : GenericRequest
    {
        /// <summary>
        /// Creates the trades request.
        /// </summary>
        /// <param name="pastSeconds"> Limit scope going back given seconds. </param>
        /// <param name="limit_trades"> Limit the number of trades returned. </param>
        public TradesRequest(double pastSeconds, int limit_trades, string currenciesTraded)
        {
            pastSeconds = TimeConversions.Nonce - pastSeconds;
            this.request = $"/v1/trades/{currenciesTraded}?timestamp={pastSeconds}&limit_trades={limit_trades}";
        }
    }
}
