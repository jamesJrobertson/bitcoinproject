﻿using HelpersController.Time;

namespace ApiController.ApiV1.Requests
{
    /// <summary>
    /// Creates a new order on bitfinex.
    /// </summary>
    class NewOrderRequest : GenericRequest
    {
        //public string request;

        //public string nonce = TimeConversions.Nonce.ToString();

        // The name of the symbol (see /symbols). (Currencypair btcusd)
        public string symbol;

        // Order size: how much you want to buy or sell
        //public double amount;
        public string amount;

        // Price to buy or sell at. Must be positive. Use random number for market orders.
        //public double price;
        public string price;

        //private string exchange = "bitfinex";
        public string exchange;

        // Either buy or sell.
        public string side;

        // Either “market” / “limit” / “stop” / “trailing-stop” / “fill-or-kill” / “exchange market” / “exchange limit” / “exchange stop” / “exchange trailing-stop” / “exchange fill-or-kill”. (type starting by “exchange ” are exchange orders, others are margin trading orders)
        public string type;

        //// true if the order should be hidden.
        //public bool? is_hidden;

        //// true if the order should be post only.Only relevant for limit orders.
        //public bool? is_postonly;

        //// 1 will post an order that will use all of your available balance.
        //public int user_all_available;

        //// Set an additional STOP OCO order that will be linked with the current order
        public bool ocoorder = false;

        //// If ocoorder is true, this field represent the price of the OCO stop order to place
        //public float buy_price_oco = 0;

        //// If ocoorder is true, this field represent the price of the OCO stop order to place
        //public float sell_price_oco = 0;

        //public NewOrderRequest(string symbol, float amount, float price, string side, string type)
        //public NewOrderRequest(string symbol, double amount, double price, string side, string type)
        ////public NewOrderRequest(string symbol, string amount, string price, string side, string type)
        //{
        //    //this.request = "/v1/order/new";
        //    request = "/v1/order/new";
        //    this.symbol = symbol;
        //    this.amount = amount;
        //    this.price = price;
        //    this.side = side;
        //    this.type = type;

        //    // below is for get requests
        //    //this.request = $"/v1/order/new/symbol={symbol}?" +
        //    //    $"amount={amount}&" +
        //    //    $"price{price}&" +
        //    //    $"exchange={exchange}&" +
        //    //    $"side{side}*" +
        //    //    $"type{type}";
        //    //         var payload = {
        //    //request: '/v1/order/new',
        //    //nonce: Date.now().toString(),
        //    //symbol: 'BTCUSD',
        //    //amount: '0.3',
        //    //price: '1000',
        //    //exchange: 'bitfinex',
        //    //side: 'sell',
        //    //type: 'exchange market'}
        //}
        //public NewOrderRequest(OrderSymbol symbol, decimal amount, decimal price, OrderExchange exchange, OrderSide side, OrderType type)
        public NewOrderRequest(string symbol, double amount, double price, string side, string type)
        {
            this.symbol = symbol;
            this.amount = amount.ToString();
            this.price = price.ToString();
            //this.amount = amount;
            //this.price = price;
            //this.exchange = EnumHelper.EnumToStr(exchange);
            //this.side = EnumHelper.EnumToStr(side);
            this.side = side;
            this.type = type;
            //this.nonce = nonce;
            this.exchange = "bitfinex";
            this.request = "/v1/order/new";
            //this.options.Add(this.symbol);
            //this.options.Add(this.amount);
            //this.options.Add(this.price);
            //this.options.Add(this.exchange);
            //this.options.Add(this.side);
            //this.options.Add(this.type);

            //string paramDict = "{\"request\": \"/v1/balances\",\"nonce\": \"" + nonce + "\"}";

            //this.request = $"/v1/trades/{currenciesTraded}?timestamp={pastSeconds}&limit_trades={limit_trades}";
            //"{\"request\":\"/v1/trades/btcusd?timestamp=1531258773.93557&limit_trades=10\",\"nonce\":\"1531258833.93557\"}"
            //"{\"request\":\"/v1/order/new/symbol=BTCUSD?nonce=1531258759.12807&amount=0.002&price=15&exchange=bitfinex&side=buy&type=exchange market\",\"nonce\":\"1531258759.12807\"}"
            //this.request = $"/v1/order/new/symbol={symbol}," +
            //    $"timestamp:{nonce}," +
            //    $"amount:{amount}," +
            //    $"price:{price}," +
            //    $"exchange:bitfinex," +
            //    $"side:{side}," +
            //    $"type:{type}";
        }

    }
}
