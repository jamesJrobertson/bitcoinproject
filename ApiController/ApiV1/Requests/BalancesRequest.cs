﻿namespace ApiController.ApiV1
{
    class BalancesRequest : GenericRequest
    {
        /// <summary>
        /// Gets balances from bitfinex.
        /// </summary>
        public BalancesRequest()
        {
            this.request = "/v1/balances";
        }
    }
}
