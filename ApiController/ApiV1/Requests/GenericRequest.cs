﻿using System.Collections;
using HelpersController.Time;

namespace ApiController.ApiV1
{
    class GenericRequest
    {
        public string request;
        public string nonce = TimeConversions.Nonce.ToString();
        public ArrayList options = new ArrayList();
    }
}
