﻿using System.Collections.Generic;
using Newtonsoft.Json;
using GlobalEntities.Enums;
using GlobalEntities.Entities;
using System;

namespace ApiController.ApiV1.Responses
{
    public class TradeResponseItem
    {
        public double timeStamp;
        public int tradeId;
        public double price;
        public double amount;
        public string exchange;
        public string type;
    }

    public class TradeResponse
    {
        public List<Trade> _trades = new List<Trade>();
        public CurrencyPair _currencyPair;
        
        public static TradeResponse FromJSON(string response, CurrencyPair currencyPair)
        {
            var returnTradeResponse = new TradeResponse();
            try
            {
                returnTradeResponse = new TradeResponse(JsonConvert.DeserializeObject<List<TradeResponseItem>>(response), currencyPair);                
            }
            // happens when internet is not connected to and bitfinex.api cannot be reached because I passed a null value in BitfinexApiV1 SendRequest
            // Just return empty TradeResponse which is handled in MenuController
            // System.ArgumentNullException: 'Value cannot be null'
            catch (System.Net.WebException)
            {
                throw;
            }            

            return returnTradeResponse;
        }

        // Empty constructor to catch connection error to bitfinex api
        public TradeResponse() { }

        private TradeResponse(List<TradeResponseItem> tradeResponseItems, CurrencyPair currencyPair)
        {
            _currencyPair = currencyPair;

            foreach (TradeResponseItem tradeResponseItem in tradeResponseItems)
            {
                var trade = new Trade()
                {
                    Price = tradeResponseItem.price,
                    CurrencyPair = currencyPair,
                    Amount = tradeResponseItem.amount
                };

                // slower than case statement
                // https://stackoverflow.com/questions/16100/how-should-i-convert-a-string-to-an-enum-in-c
                var parseSuccesful = Enum.TryParse(tradeResponseItem.type, out TradeType tempTrade);
                trade.TradeType = tempTrade;

                _trades.Add(trade);
            }
        }
    }
}
