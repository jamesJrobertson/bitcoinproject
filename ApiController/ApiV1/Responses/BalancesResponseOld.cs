﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System.Globalization;
using GlobalEntities.Entities;
using GlobalEntities.Enums;

namespace ApiController.ApiV1.Responses
{
    /// <summary>
    /// The class to make the response to.
    /// </summary>
    public class BalanceResponseItem
    {
        public string type;
        public string currency;
        public string amount;
        public string available;
    }

    public class BalanceResponseOld
    {
        public List<Balance> AllBalances = new List<Balance>();

        public static BalanceResponseOld FromJSON(string response)
        {
            return new BalanceResponseOld(JsonConvert.DeserializeObject<List<BalanceResponseItem>>(response));
        }
        private BalanceResponseOld(List<BalanceResponseItem> balanceResponseItems)
        {
            foreach (BalanceResponseItem balanceResponseItem in balanceResponseItems)
            {
                var currentBalance = new Balance()
                {
                    AvailableAmount = double.Parse(balanceResponseItem.available, CultureInfo.InvariantCulture),
                    TotalAmount = double.Parse(balanceResponseItem.amount, CultureInfo.InvariantCulture)
                };

                switch (balanceResponseItem.type)
                {
                    case "trading":
                        currentBalance.WalletType = WalletType.trading;
                        break;
                    case "deposit":
                        currentBalance.WalletType = WalletType.deposit;
                        break;
                    case "exchange":
                        currentBalance.WalletType = WalletType.exchange;
                        break;
                }

                switch (balanceResponseItem.currency)
                {
                    case "usd":
                        currentBalance.CurrencyType = Currencies.usd;
                        break;

                    case "btc":
                        currentBalance.CurrencyType = Currencies.btc;
                        break;
                }

                AllBalances.Add(currentBalance);
            }
        }
    }
}
