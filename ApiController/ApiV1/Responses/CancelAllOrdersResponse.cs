﻿using GlobalEntities.Entities;
using Newtonsoft.Json;

namespace ApiController.ApiV1.Responses
{
    public class CancelAllOrdersResponse: BaseProperties
    {
        public string Result { get; set; }
            
        public static CancelAllOrdersResponse FromJSON(string response)                
        {
            return (JsonConvert.DeserializeObject<CancelAllOrdersResponse>(response));
        }

        public override string ToString()
        {            
            return $"Result - {Result}, TimeStampUtc - {TimeStampUtc}";
        }
    }
}
