﻿using GlobalEntities.Entities;
using GlobalEntities.Enums;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiController.ApiV1.Responses
{
    /// <summary>
    /// The class to make the response to.
    /// </summary>
    
    //[Table("Orders")]
    public class NewOrderResponseItem
    {
        //[Key]
        public long orderId;
        public string symbol;
        public string exchange;
        public float price;
        public float avg_execution_price;
        public string side;
        public string type;
        public DateTime timestamp;
        public bool is_live;
        public bool is_cancelled;
        public bool is_hidden;
        public long oco_order;
        public bool was_forced;
        public float executed_amount;
        public float remaining_amount;
        public float original_amount;
    }

    public class NewOrderResponse
    {
        public Order _order;

        public static NewOrderResponse FromJSON(string response)
        {
            return new NewOrderResponse(JsonConvert.DeserializeObject<NewOrderResponseItem>(response));
        }

        private NewOrderResponse(NewOrderResponseItem newOrderResponseItem)
        {
            var order = new Order()
            {
                OrderId = newOrderResponseItem.orderId,
                Symbol = newOrderResponseItem.symbol,
                Exchange = newOrderResponseItem.exchange,
                Price = newOrderResponseItem.price,
                AvgExecutionPrice = newOrderResponseItem.avg_execution_price,                                
                IsLive = newOrderResponseItem.is_live,
                IsCancelled = newOrderResponseItem.is_cancelled,
                IsHidden = newOrderResponseItem.is_hidden,
                OcoOrder = newOrderResponseItem.oco_order,
                WasForced = newOrderResponseItem.was_forced,
                ExecutedAmount = newOrderResponseItem.executed_amount,
                RemainingAmount = newOrderResponseItem.remaining_amount,
                OriginalAmount = newOrderResponseItem.original_amount
            };
            
            // slower than case statement
            // https://stackoverflow.com/questions/16100/how-should-i-convert-a-string-to-an-enum-in-c
            var parseSuccesful = Enum.TryParse(newOrderResponseItem.type, out TradeType tempTrade);
            order.Side = tempTrade;

            // slower than case statement
            // https://stackoverflow.com/questions/16100/how-should-i-convert-a-string-to-an-enum-in-c
            parseSuccesful = Enum.TryParse(newOrderResponseItem.side, out OrderType tempOrderType);
            order.OrderType = tempOrderType;
        }
    }

}
