﻿using System;
using System.Data.SqlClient;
using Context.BitcoinDatabase;

namespace Context.Sql
{
    public class Sql
    {
        public SqlConnectionStringBuilder GetLoginInfo(string initialCatalog, string connectionString)
        {
            // Build connection string
            return new SqlConnectionStringBuilder
            {
                DataSource = "(localdb)\\mssqllocaldb",
                UserID = "james",
                Password = "",
                InitialCatalog = initialCatalog,
                ConnectionString = connectionString
            };
        }

        public BitcoinContext RunSql()
        {
            try
            {
                SqlConnectionStringBuilder builder = GetLoginInfo("bitcoin", "Server = (localdb)\\mssqllocaldb; Database = Context.BitcoinDatabase.BitcoinContext; Trusted_Connection = True;");
                return new BitcoinContext(builder.ConnectionString);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw (new Exception("Sql context failed to be created."));
            }
        }
    }
}
