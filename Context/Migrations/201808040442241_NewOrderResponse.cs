namespace Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewOrderResponse : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NewOrderResponses",
                c => new
                    {
                        GuidId = c.Guid(nullable: false),
                        order_id = c.String(),
                        id = c.String(),
                        symbol = c.String(),
                        exchange = c.String(),
                        price = c.String(),
                        avg_execution_price = c.String(),
                        side = c.String(),
                        type = c.String(),
                        timestamp = c.String(),
                        is_live = c.String(),
                        is_cancelled = c.String(),
                        is_hidden = c.String(),
                        was_forced = c.String(),
                        executed_amount = c.String(),
                        remaining_amount = c.String(),
                        original_amount = c.String(),
                    })
                .PrimaryKey(t => t.GuidId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.NewOrderResponses");
        }
    }
}
