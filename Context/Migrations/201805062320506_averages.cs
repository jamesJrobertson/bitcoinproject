namespace Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class averages : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Averages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AveragePrice = c.Double(nullable: false),
                        IsBuy = c.Boolean(nullable: false),
                        TotalAmount = c.Double(nullable: false),
                        TotalCount = c.Double(nullable: false),
                        TimeStampUtc = c.DateTime(nullable: false),
                        TimeStampLocal = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Averages");
        }
    }
}
