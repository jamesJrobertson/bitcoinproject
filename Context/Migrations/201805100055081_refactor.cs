namespace Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class refactor : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CurrencyPairs",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FirstCurrency = c.Int(nullable: false),
                        SecondCurrency = c.Int(nullable: false),
                        TimeStampUtc = c.DateTime(nullable: false),
                        TimeStampLocal = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Trades", "AverageId", c => c.Guid());
            AddColumn("dbo.Trades", "CurrencyPair_Id", c => c.Guid());
            CreateIndex("dbo.Trades", "AverageId");
            CreateIndex("dbo.Trades", "CurrencyPair_Id");
            AddForeignKey("dbo.Trades", "AverageId", "dbo.Averages", "Id");
            AddForeignKey("dbo.Trades", "CurrencyPair_Id", "dbo.CurrencyPairs", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Trades", "CurrencyPair_Id", "dbo.CurrencyPairs");
            DropForeignKey("dbo.Trades", "AverageId", "dbo.Averages");
            DropIndex("dbo.Trades", new[] { "CurrencyPair_Id" });
            DropIndex("dbo.Trades", new[] { "AverageId" });
            DropColumn("dbo.Trades", "CurrencyPair_Id");
            DropColumn("dbo.Trades", "AverageId");
            DropTable("dbo.CurrencyPairs");
        }
    }
}
