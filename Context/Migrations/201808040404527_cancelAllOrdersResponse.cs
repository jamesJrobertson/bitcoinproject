namespace Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cancelAllOrdersResponse : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CancelAllOrdersResponses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Result = c.String(),
                        TimeStampUtc = c.DateTime(nullable: false),
                        TimeStampLocal = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CancelAllOrdersResponses");
        }
    }
}
