namespace Context.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Balance_Trade : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Trades",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Price = c.Double(nullable: false),
                        Amount = c.Double(nullable: false),
                        TradeType = c.Int(nullable: false),
                        TimeStampUtc = c.DateTime(nullable: false),
                        TimeStampLocal = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Balances", "CurrencyType", c => c.Int(nullable: false));
            AddColumn("dbo.Balances", "WalletType", c => c.Int(nullable: false));
            AddColumn("dbo.Balances", "AvailableAmount", c => c.Double(nullable: false));
            AddColumn("dbo.Balances", "TotalAmount", c => c.Double(nullable: false));
            DropColumn("dbo.Balances", "USD");
            DropColumn("dbo.Balances", "BTC");
            DropColumn("dbo.Balances", "AvailableUSD");
            DropColumn("dbo.Balances", "AvailableBTC");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Balances", "AvailableBTC", c => c.Double(nullable: false));
            AddColumn("dbo.Balances", "AvailableUSD", c => c.Double(nullable: false));
            AddColumn("dbo.Balances", "BTC", c => c.Double(nullable: false));
            AddColumn("dbo.Balances", "USD", c => c.Double(nullable: false));
            DropColumn("dbo.Balances", "TotalAmount");
            DropColumn("dbo.Balances", "AvailableAmount");
            DropColumn("dbo.Balances", "WalletType");
            DropColumn("dbo.Balances", "CurrencyType");
            DropTable("dbo.Trades");
        }
    }
}
