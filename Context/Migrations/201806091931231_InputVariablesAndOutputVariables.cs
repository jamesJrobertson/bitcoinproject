namespace Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InputVariablesAndOutputVariables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InputVariables",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SecondLimit = c.Int(nullable: false),
                        BuyLimitRule = c.Double(nullable: false),
                        SellBoughtCurrencyLimit = c.Double(nullable: false),
                        ShortLimitRule = c.Double(nullable: false),
                        SellShortCurrencyLimit = c.Double(nullable: false),
                        USDToTrade = c.Double(nullable: false),
                        Method = c.Int(nullable: false),
                        TimeStampUtc = c.DateTime(nullable: false),
                        TimeStampLocal = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OutputVariables",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FinalMoney = c.Double(nullable: false),
                        MostMoney = c.Double(nullable: false),
                        LeastMoney = c.Double(nullable: false),
                        Trades_Buys = c.Int(nullable: false),
                        Trades_Shorts = c.Int(nullable: false),
                        Trades_BuysSold = c.Int(nullable: false),
                        Trades_ShortsSold = c.Int(nullable: false),
                        PercentLostToTrades = c.Double(nullable: false),
                        TimeSpan = c.Long(nullable: false),
                        InputVariablesId = c.Guid(nullable: false),
                        TimeStampUtc = c.DateTime(nullable: false),
                        TimeStampLocal = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InputVariables", t => t.InputVariablesId, cascadeDelete: true)
                .Index(t => t.InputVariablesId);
            
            DropColumn("dbo.PastDatas", "TotalCount");
            DropColumn("dbo.PastDatas", "TotalTime");
            DropColumn("dbo.PastDatas", "WeightedPrice");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PastDatas", "WeightedPrice", c => c.Double(nullable: false));
            AddColumn("dbo.PastDatas", "TotalTime", c => c.Double(nullable: false));
            AddColumn("dbo.PastDatas", "TotalCount", c => c.Double(nullable: false));
            DropForeignKey("dbo.OutputVariables", "InputVariablesId", "dbo.InputVariables");
            DropIndex("dbo.OutputVariables", new[] { "InputVariablesId" });
            DropTable("dbo.OutputVariables");
            DropTable("dbo.InputVariables");
        }
    }
}
