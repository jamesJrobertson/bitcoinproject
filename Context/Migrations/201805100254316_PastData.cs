namespace Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class PastData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PastDatas",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    TimeStampUnix = c.Int(nullable: false),
                    TotalAmount = c.Double(nullable: false),
                    TotalUSD = c.Double(nullable: false),
                    WeightedUSD = c.Double(nullable: false),
                    TotalCount = c.Double(nullable: false),
                    TotalTime = c.Double(nullable: false),
                    WeightedPrice = c.Double(nullable: false),
                    TimeStampUtc = c.DateTime(nullable: false),
                    TimeStampLocal = c.DateTime(nullable: false),
                    CurrencyPair_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CurrencyPairs", t => t.CurrencyPair_Id)
                .Index(t => t.CurrencyPair_Id);
        }

        public override void Down()
        {
            DropForeignKey("dbo.PastDatas", "CurrencyPair_Id", "dbo.CurrencyPairs");
            DropIndex("dbo.PastDatas", new[] { "CurrencyPair_Id" });
            DropTable("dbo.PastDatas");
        }
    }
}
