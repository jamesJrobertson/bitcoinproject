namespace Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Balances",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        USD = c.Double(nullable: false),
                        BTC = c.Double(nullable: false),
                        AvailableUSD = c.Double(nullable: false),
                        AvailableBTC = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Balances");
        }
    }
}
