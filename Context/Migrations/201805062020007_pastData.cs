namespace Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pastData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PastDatas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TimeStampUnix = c.Int(nullable: false),
                        TimeStampUtc = c.DateTime(nullable: false),
                        TimeStampLocal = c.DateTime(nullable: false),
                        TotalAmount = c.Double(nullable: false),
                        TotalUSD = c.Double(nullable: false),
                        WeightedUSD = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PastDatas");
        }
    }
}
