namespace Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlgorithmState : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AlgorithmStates",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        BitcoinState = c.Int(nullable: false),
                        TimeStampUtc = c.DateTime(nullable: false),
                        TimeStampLocal = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AlgorithmStates");
        }
    }
}
