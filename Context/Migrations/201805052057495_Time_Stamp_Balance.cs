namespace Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Time_Stamp_Balance : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Balances", "TimeStampUtc", c => c.DateTime(nullable: true));
            AddColumn("dbo.Balances", "TimeStampLocal", c => c.DateTime(nullable: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Balances", "TimeStampLocal");
            DropColumn("dbo.Balances", "TimeStampUtc");
        }
    }
}
