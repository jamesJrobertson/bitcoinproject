﻿using System.Data.Entity;
using BitfinexApi;
using GlobalEntities.Entities;

namespace Context.BitcoinDatabase
{
    public class BitcoinContext : DbContext
    {
        // Empty constuctor needed for entity framework.
        public BitcoinContext() { }

        public BitcoinContext(string connectionString)
        {
            Database.CreateIfNotExists();
            Database.Connection.ConnectionString = connectionString;
        }

        public DbSet<Average>                 Averages                { get; set; }
        public DbSet<Balance>                 Balances                { get; set; }        
        public DbSet<CancelAllOrdersResponse> CancelAllOrdersResponse { get; set; }        
        public DbSet<InputVariables>          InputVariables          { get; set; }
        public DbSet<NewOrderResponse>        NewOrderResponses       { get; set; }
        public DbSet<OutputVariables>         OutputVariables         { get; set; }
        //public DbSet<Trade>                   PastBitcoinTrades       { get; set; }
        public DbSet<PastData>                PastData                { get; set; }
        public DbSet<AlgorithmState>          AlgorithmState          { get; set; }
        public DbSet<Trade>                   Trades                  { get; set; }        
    }
}
