﻿using System;
using System.Collections.Generic;
using System.Linq;
using GlobalEntities.Entities;
using GlobalEntities.Enums;
using ProjectController.Output;
using Context.BitcoinDatabase;
using AnalysisController.HistoricalDataReader;
using Queries.PastDataEntity;
using AnalysisController.DataAnalysis;
using OxyPlot;
using System.Windows.Forms;
using GrapherController;
using AnalysisController.StatsCalculator;
using Queries.BitcoinAveragesEntity;
using Queries.StateEntity;
using HelpersController.Logger;
using BitfinexApi;
using Context.Sql;

namespace ProjectController.ApplicationManagment
{
    public class MenuController
    {
        private BitcoinContext _context;
        private BitfinexApiV1 _readApi;
        private BitfinexApiV1 _writeApi;
        private BitcoinAveragesQueries _bitcoinAveragesQueries;
        private PastDataQueries _pastDataQueries;

        private System.Threading.Timer _timer;

        private List<Average> _pastMinuteAverage = new List<Average>(new Average[2]);
        private double pastBuySlope;
        private double pastSellSlope;

        private InputVariables _inputVariables = new InputVariables()
        {
            SecondLimit = 86400, // 1 day
            BuyLimitRule = 5e-3,
            SellBoughtCurrencyLimit = 5e-3,
            ShortLimitRule = -5e-5,
            SellShortCurrencyLimit = -5e-5,
            USDToTrade = 100,
            Method = Algorithm.poly1 // first order polynomial (slope)
        };

        // Note: Algorithm currently trades at market price        
        private decimal _randomPriceToSetForMarketTrading = 1m; // not actually used by API if trading at market price

        readonly double _limitDataPercent = 0.7; // need 70% of data to do calculations

        public MenuController(BitcoinContext context, BitfinexApiV1 writeApi, BitfinexApiV1 readApi)
        {
            _context = context;
            _writeApi = writeApi;
            _readApi = readApi;
            _bitcoinAveragesQueries = new BitcoinAveragesQueries(_context);
            _pastDataQueries = new PastDataQueries(_context);
        }

        /// <summary>
        /// Interputs the users input and decides what to do next.
        /// </summary>
        /// <param name="keyPressed"> The <see cref="ConsoleKey"/> the user pressed. </param>
        public void InterpretMenuChoice(MenuChoiceEnum keyPressed)
        {
            switch (keyPressed)
            {
                case MenuChoiceEnum.getBalances:
                    var balanceResponse = _readApi.GetBalances();                    
                    ConsoleManager.PrintBalances(balanceResponse);
                    _context.Balances.AddRange(balanceResponse.AllBalances);
                    _context.SaveChanges();
                    break;

                case MenuChoiceEnum.printTrades:
                    ConsoleManager.PrintTrades(_readApi.GetTrades(double.MaxValue, int.MaxValue));                    
                    break;

                case MenuChoiceEnum.makePastDatabase:
                    // Confirm if we want to make the past database
                    if (ConsoleManager.ConfirmationMenu() == MenuChoiceEnum.yes)
                    {
                        ConsoleManager.WriteMessage("Creating past database");
                        new PastDataManager(_context).CreatePastDatabase();
                    }
                    break;

                // Graphs USD returned vs time for specific variable combination
                case MenuChoiceEnum.analayzePastData:
                    {
                        //var dataPoints = _pastDataQueries.GetWeightedUsdAndTimeStampUnix();
                        // Use our data and pass in the first 80% of the points
                        (var pastBuyDataPoints, var pastSellDataPoints) = new PresentDataAnalyzer().GetOurData(_context);
                        var eightyPecentOfPastBuyDataPoints = pastBuyDataPoints.GetRange(0, (int)Math.Round(pastBuyDataPoints.Count * 0.8));
                        (var outputVariables, var money, var slopes) = new PastDataAnalyzer().AnalyzePastData(eightyPecentOfPastBuyDataPoints, _inputVariables, _limitDataPercent, true);

                        var i = 0;
                        var moneyGraphPoints = new List<DataPoint>();
                        var slopeGraphPoints = new List<DataPoint>();
                        var bitcoinPriceGraphPoints = new List<DataPoint>();
                        foreach (var point in eightyPecentOfPastBuyDataPoints)
                        {
                            moneyGraphPoints.Add(new DataPoint(point.XValue, money[i]));
                            slopeGraphPoints.Add(new DataPoint(point.XValue, slopes[i]));
                            bitcoinPriceGraphPoints.Add(new DataPoint(point.XValue, point.YValue));
                            i += 1;
                        }

                        var grapher = new Grapher();
                        grapher.graphValues(moneyGraphPoints, slopeGraphPoints, "USD vs slope", "Unix Time", "USD", "Slope");
                        Application.EnableVisualStyles();
                        Application.Run(grapher);                                                
                        
                        grapher = new Grapher();
                        grapher.graphValues(moneyGraphPoints, bitcoinPriceGraphPoints, "USD and bitcoin price", "Unix Time", "USD", "Bitcoin Price");
                        Application.EnableVisualStyles();
                        Application.Run(grapher);

                        break;
                    }
                case MenuChoiceEnum.findBestVariableCombination:
                    {
                        // use the past data (2018-01 to 2018-03)
                        //var dataPoints = _pastDataQueries.GetWeightedUsdAndTimeStampUnix().ToList();
                        //Console.WriteLine("Validation set = past data (2018-01 to 2018-03)");
                        //new FindBestVariableCombination(_context, _limitDataPercent).LoopPastDataAnalyzer(dataPoints);

                        // use our data (2018-08 to present)
                        Console.WriteLine("Validation set = our data (2018-08 to 80% present)");
                        (var pastBuyDataPoints, var pastSellDataPoints) = new PresentDataAnalyzer().GetOurData(_context);

                        // retrieve 80% of array from start
                        new FindBestVariableCombination(_context, _limitDataPercent).LoopPastDataAnalyzer(pastBuyDataPoints.GetRange(0, (int)Math.Round(pastBuyDataPoints.Count * 0.8))); 
                        break;
                    }

                case MenuChoiceEnum.fixBrokenTables:
                    // Confirm if we want to make the past database
                    if (ConsoleManager.ConfirmationMenu() == MenuChoiceEnum.yes)
                    {
                        ConsoleManager.WriteMessage("Fixing broken tables you idiot");
                        //new PastAveragesManager(_context).RecreateAveragesTable();
                        new PastInputVariablesManager(_context).RecreateInputVariablesTable();
                        new PastOutputVariablesManager(_context).RecreateOutputVariablesTable();
                    }
                    break;

                case MenuChoiceEnum.cancelAllOrders:

                    var response = _writeApi.CancelAllOrders();
                    Console.WriteLine(response.ToString());
                    _context.CancelAllOrdersResponse.Add(response);
                    //_context.SaveChanges(); // Don't need to save here because setState saves the database
                    var setState = new SetState();
                    setState.Execute(_context, BitcoinStates.pending);

                    break;

                case MenuChoiceEnum.analyzeOurData:
                    {
                        (var pastBuyDataPoints, var pastSellDataPoints) = new PresentDataAnalyzer().GetOurData(_context);
                        // Just going to analyze the pastBuyDataPoints for now. Minimal gain to analyze both
                        // use last 20% of points              
                        var eightyPercentPoint = (int)Math.Round(pastBuyDataPoints.Count * 0.8);
                        var twentyPercentPastBuyDataPoints = pastBuyDataPoints.GetRange(eightyPercentPoint, pastBuyDataPoints.Count - eightyPercentPoint);
                        (var outputVariables, var money, var slopes) = new PastDataAnalyzer().AnalyzePastData(twentyPercentPastBuyDataPoints, _inputVariables, _limitDataPercent, true);

                        var i = 0;
                        var moneyGraphPoints = new List<DataPoint>();
                        var slopeGraphPoints = new List<DataPoint>();
                        var pastBuyGraphPoints = new List<DataPoint>();
                        foreach (var point in twentyPercentPastBuyDataPoints)
                        {
                            moneyGraphPoints.Add(new DataPoint(point.XValue, money[i]));
                            slopeGraphPoints.Add(new DataPoint(point.XValue, slopes[i]));
                            pastBuyGraphPoints.Add(new DataPoint(point.XValue, point.YValue));
                            i += 1;
                        }

                        var grapher = new Grapher();
                        grapher.graphValues(moneyGraphPoints, slopeGraphPoints, "USD and slope", "Unix Time", "USD", "Slope");
                        Application.EnableVisualStyles();
                        Application.Run(grapher);
                        grapher = new Grapher();
                        grapher.graphValues(moneyGraphPoints, pastBuyGraphPoints, "USD and bitcoin price", "Unix Time", "USD", "Bitcoin Price");
                        Application.EnableVisualStyles();
                        Application.Run(grapher);

                        break;
                    }

                case MenuChoiceEnum.debuggingOption:
                    
                    break;

                case MenuChoiceEnum.indefinitelyRecordDataOnly:

                    // Editing according to here, trying to prevent hanging
                    // https://stackoverflow.com/questions/4962172/why-does-a-system-timers-timer-survive-gc-but-not-system-threading-timer
                    _timer = null;
                    _timer = new System.Threading.Timer(RecordAverages);
                    _timer.Change(0, 60000);
                    break;

                case MenuChoiceEnum.runAlgorithm:

                    // Insert code to also record the trades to table
                    // Place in infinite loop / timer
                    // Editing according to here, trying to prevent hanging
                    // https://stackoverflow.com/questions/4962172/why-does-a-system-timers-timer-survive-gc-but-not-system-threading-timer
                    _timer = null;
                    _timer = new System.Threading.Timer(LoopAlgorithm);
                    _timer.Change(0, 60000);
                    break;

                case MenuChoiceEnum.exit:
                    //new AuthenticationController.LogOut(); // TODO
                    return;

                default:
                    break;
            }
            InterpretMenuChoice(ConsoleManager.DisplayMenu());
        }

        private void LoopAlgorithm(Object o)
        {
            BitcoinContext _context = new Sql().RunSql();
            RecordAverages(null);
            (pastBuySlope, pastSellSlope) = new PresentDataAnalyzer().BuySellBitcoinOrDoNothing(_context, _writeApi, _readApi, _inputVariables,
                        _randomPriceToSetForMarketTrading, _limitDataPercent, pastBuySlope, pastSellSlope);
            _context.Dispose(); // unsure if this is needed
        }

        private void RecordAverages(Object o)
        {
            ConsoleManager.WriteMessage($"RecordAverages() called at {DateTime.Now.ToLongTimeString()}\n");
            BitcoinContext _context = new Sql().RunSql();            

            try
            {
                var trades = _readApi.GetTrades(60, int.MaxValue); // <- error is propogated back to here on a failed connection                
                
                var averages = new Statistics().CreateAverages(trades);
                (averages, _pastMinuteAverage) = new Statistics().fixAveragesIfMissingData(averages, _pastMinuteAverage);

                // Occurs if trying to write a null value to the table
                //SqlException: The incoming tabular data stream(TDS) remote procedure call(RPC) protocol stream is incorrect.Parameter 4("@1"): The supplied value is not a valid instance of data type float.Check the source data for invalid values. An example of an invalid value is data of numeric type with scale greater than precision.
                // Catch the case where there first X times running the algorithm there is no trades (don't write to table then)
                if (averages[1].TotalCount > 0 && averages[0].TotalCount > 0)
                {
                    _context.Averages.AddRange(averages);
                    _context.SaveChanges();
                }
            }
            catch (System.Net.WebException ex)
            {
                Console.WriteLine("failed connection - " + DateTime.Now);
                new LoggerManager(ex).Logger();
            }

            _context.Dispose(); // unsure if this is needed            
        }
    }
}
