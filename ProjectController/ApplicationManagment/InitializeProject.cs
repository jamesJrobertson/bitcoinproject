﻿using BitfinexApi;
using Context.BitcoinDatabase;
using Context.Sql;
using ProjectController.ApplicationManagment;
using ProjectController.Output;
using ProjectController1.ApplicationManagment;
using System;

namespace InitializeProject
{
    /// <summary>
    /// This class is used to new up the project and any dependencies needed in the project.
    /// </summary>
    public static class ProjectManager
    {
        // Static global entities, classes passed throughout the project.
        public static BitfinexApiV1 _readOnlyApi = new AuthenticationController().ReadOnlyLogin();        
        public static BitfinexApiV1 _writeOnlyApi = new AuthenticationController().WriteOnlyLogin();        
        public static BitcoinContext _context = new Sql().RunSql();
        
        [STAThread]
        static void Main(string[] args)
        {
            // Stops the functionality that stops the program when clicking in it
            DisableQuickEditMode.Go();
            // Start the menu.                        
            new MenuController(_context, _writeOnlyApi, _readOnlyApi).InterpretMenuChoice(ConsoleManager.DisplayMenu());                   
        }
    }
}
