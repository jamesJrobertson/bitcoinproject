﻿using BitfinexApi;

namespace ProjectController.ApplicationManagment
{
    /// <summary>
    /// This class stores the api keys required to interact with the bitfinex exchange.
    /// The keys will vary on what permissions are required.
    /// </summary>
    public interface IAuthenticationController
    {
        /// <summary>
        /// Creates a read only instance of the bitfinex api.
        /// </summary>
        /// <returns> Instance of the api. <see cref="BitfinexApiV1"/> </returns>
        BitfinexApiV1 ReadOnlyLogin();

        /// <summary>
        /// Creates a write only instance of the bitfinex api.
        /// </summary>
        /// <returns> Instance of the api. <see cref="BitfinexApiV1"/> </returns>
        BitfinexApiV1 WriteOnlyLogin();
    }
}
