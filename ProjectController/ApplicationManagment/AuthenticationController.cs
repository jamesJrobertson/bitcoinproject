﻿using BitfinexApi;

namespace ProjectController.ApplicationManagment
{ 
    public class AuthenticationController : IAuthenticationController
    {
        /// <summary>
        /// This has read only permissions:
        /// Account Info, Account History, Orders, Margin Trading, Margin Funding, Wallets, Withdraw.
        /// </summary>
        private readonly string READ_ONLY_PERMISSIONS_API_KEY = "iiz7EzrBBtkhEMDAtE1tBQDMgZX7dho38wxbpfyYhR5";
        private readonly string READ_ONLY_PERMISSIONS_API_KEY_SECRET = "0hNj9oglIZtmMTTeYN9J5dW1dos2sL7pmSzU4daIaoH";

        /// <summary>
        /// This has write only permissions:
        /// Orders, Margin Trading, Margin Funding, Wallets.
        /// </summary>
        private readonly string WRITE_ONLY_PERMISSIONS_API_KEY = "480NwXFCWJ6KphfimAYkDTXNO8zO1EyPVIQabwYY35r";
        private readonly string WRITE_ONLY_PERMISSIONS_API_KEY_SECRET = "0cqy3Nh4qcR5DZeiFA3xFWvOscOyVv80SAjgAqRXgIL";

        // <inheritdoc />
        public BitfinexApiV1 ReadOnlyLogin() => new BitfinexApiV1(READ_ONLY_PERMISSIONS_API_KEY, READ_ONLY_PERMISSIONS_API_KEY_SECRET);

        // <inheritdoc />
        public BitfinexApiV1 WriteOnlyLogin() => new BitfinexApiV1(WRITE_ONLY_PERMISSIONS_API_KEY, WRITE_ONLY_PERMISSIONS_API_KEY_SECRET);

        /// <summary>
        /// Does any final updates / saves and logs out of the api (invalidates it).
        /// </summary>
        public void LogOut()
        {
            // TODO - invalidate the api. Might need to be moved to the api.
        }
    }
}
