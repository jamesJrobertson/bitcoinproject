﻿using System;
using BitfinexApi;
using GlobalEntities.Enums;

namespace ProjectController.Output
{
    /// <summary>
    /// Class to manage all printing to console.
    /// </summary>
    public static class ConsoleManager
    {
        /// <summary>
        /// Writes the given message to the console.
        /// </summary>
        /// <param name="message"> The message to be displayed, defaults to an empty string if no message given. </param>
        /// <param name="newLine"> Flag to add a new line or not defaults to new line. </param>
        public static void WriteMessage(string message = "", bool newLine = true)
        {
            if (newLine)
            {
                Console.WriteLine(message);
            }
            else
            {
                Console.Write(message);
            }
        }

        /// <summary>
        /// Waits for the users input.
        /// </summary>
        /// <returns> The <see cref=ConsoleKey"/> the user pressed. </returns>
        public static ConsoleKey ReadKey()
        {
            return Console.ReadKey().Key;
        }

        /// <summary>
        /// Writes the menu to the console.
        /// </summary>
        /// <returns> The <see cref=ConsoleKey"/> the user pressed. </returns>
        public static MenuChoiceEnum DisplayMenu()
        {
            WriteMessage("1 - Indefinitely record average of bitcoin trades");
            WriteMessage("2 - Run algorithm (record averages + buy/sell)");
            WriteMessage("3 - Cancel all orders");
            WriteMessage("4 - Get balance");
            WriteMessage("5 - Get current trades");            
            WriteMessage("6 - Graph return over validation set");
            WriteMessage("7 - Graph return over test set (test algorithm)");
            WriteMessage("8 - Find best combination of variables using validation set");
            WriteMessage("W - Debugging / Testing Option");
            WriteMessage("X - Make past database");
            WriteMessage("Z - Recreate averages, input and output variables tables");            
            WriteMessage("ESC - To logout and quit");

            return (MenuChoiceEnum) ReadKey();
        }

        /// <summary>
        /// Writes yes/no confirmation to the menu
        /// </summary>
        /// <returns> The <see cref=ConsoleKey"/> the user pressed. </returns>
        public static MenuChoiceEnum ConfirmationMenu()
        {
            WriteMessage("Are you 100% super duper sure? (y/n)");

            return (MenuChoiceEnum)ReadKey();
        }

        /// <summary>
        /// Prints the bitcoin and usd total balances.
        /// </summary>
        /// <param name="balanceResponse"> Balances from the account. </param>
        public static void PrintBalances(BalancesResponse balanceResponse)
        {
            balanceResponse.AllBalances.ForEach(b => WriteMessage(b.ToString()));
        }

        /// <summary>
        /// Prints the given trades.
        /// </summary>
        /// <param name="tradeResponse"> Trades to print. </param>
        public static void PrintTrades(TradeResponse tradeResponse)
        {
            tradeResponse._trades.ForEach(t => WriteMessage(t.ToString()));
        }
    }
}
