﻿namespace GlobalEntities.Enums
{    
    public enum BitcoinStates
    {
        boughtBitcoin,
        shortedBitcoin,
        buying,
        shorting,
        sellingBought,
        sellingShort,
        pending
    }
}
