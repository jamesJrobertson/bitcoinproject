﻿using System;

namespace GlobalEntities.Enums
{
    public enum MenuChoiceEnum
    {
        indefinitelyRecordDataOnly  = ConsoleKey.D1,
        runAlgorithm                = ConsoleKey.D2,
        cancelAllOrders             = ConsoleKey.D3,
        getBalances                 = ConsoleKey.D4,
        printTrades                 = ConsoleKey.D5,
        analayzePastData            = ConsoleKey.D6,
        analyzeOurData              = ConsoleKey.D7,
        findBestVariableCombination = ConsoleKey.D8,        
        debuggingOption             = ConsoleKey.W,
        makePastDatabase            = ConsoleKey.X,
        fixBrokenTables             = ConsoleKey.Z,
        yes                         = ConsoleKey.Y,
        no                          = ConsoleKey.N,
        exit                        = ConsoleKey.Escape
    }
}
