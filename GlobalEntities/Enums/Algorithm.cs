﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalEntities.Enums
{
    public enum Algorithm
    {
        futurePriceEstimate,
        poly1, // first order polynomial (slope)
        poly2,
        poly3,
    }
}
