﻿namespace GlobalEntities.Enums
{
    public enum TradeType
    {
        buy,
        sell,
        unknown
    }
}
