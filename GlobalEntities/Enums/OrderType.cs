﻿using System.ComponentModel;

namespace GlobalEntities.Enums
{
    /// <summary>
    /// User in bitfinex api for orders.
    /// Note: Margin is when you borrow money from your broker to buy or sell more stock than you can afford.
    /// Exchange is when you use your own money.
    /// </summary>
    public enum OrderType
    {
        // A margin order in which a buy or sell order to be executed immediately at current market prices.
        market,

        // A margin order to buy/sell at a specified price or better.
        limit,

        // A margin order to sell or close your position once the market reaches a certain price.
        stop,

        // A margin stop order that can be set to execute once the market goes against you by a defined price,
        // called the price difference. Trailing–stop sell orders are used to maximize and protect profit 
        // as a stock's price rises and limit losses when its price falls.
        [Description("trailing-stop")]
        trailingStop,

        // A margin order that is a limit order that must be filled in its entirety or canceled (killed).
        // The purpose of a fill or kill order is to ensure that a position is entered at a desired price.
        [Description("fill-or-kill")]
        fillOrKill,

        // A exchange order in which a buy or sell order to be executed immediately at current market prices.
        [Description("exchange market")]
        exchangeMarket,

        // A exchange order to buy/sell at a specified price or better.
        [Description("exchange limit")]
        exchangeLimit,

        // A exchange order to sell or close your position once the market reaches a certain price.
        [Description("exchange stop")]
        exchangeStop,

        // A exchange stop order that can be set to execute once the market goes against you by a defined price,
        // called the price difference. Trailing–stop sell orders are used to maximize and protect profit 
        // as a stock's price rises and limit losses when its price falls.
        [Description("exchange trailing-stop")]
        exchangeTrailingStop,

        // A exchange order that is a limit order that must be filled in its entirety or canceled (killed).
        // The purpose of a fill or kill order is to ensure that a position is entered at a desired price.
        [Description("exchange fill-or-kill")]
        exchangeFillOrKill
    }
}
