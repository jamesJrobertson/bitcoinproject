﻿using System;
using GlobalEntities.Enums;

namespace GlobalEntities.Entities
{
    public class Trade : BaseProperties
    {
        public Guid? AverageId { get; set; }
        public virtual Average Average { get; set; }

        public CurrencyPair CurrencyPair { get; set; }
        public double Price { get; set; }
        public double Amount { get; set; }
        public TradeType TradeType { get; set; }

        public override string ToString()
        {
            return $"Symbol - {CurrencyPair.ToString()}, TimeStampLocal - {TimeStampLocal}, Price - {Price}, Amount - {Amount}, {TradeType}";
        }
    }
}
