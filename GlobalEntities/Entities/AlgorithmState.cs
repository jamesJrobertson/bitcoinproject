﻿using GlobalEntities.Enums;

namespace GlobalEntities.Entities
{
    public class AlgorithmState: BaseProperties
    {
        public BitcoinStates BitcoinState { get; set; }
    }
}
