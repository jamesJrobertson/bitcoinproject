﻿using System;

namespace GlobalEntities.Entities
{
    public class BaseProperties
    {
        public Guid Id { get; set; }
        public DateTime TimeStampUtc { get; set; }
        public DateTime TimeStampLocal { get; set; }

        public BaseProperties()
        {
            Id = Guid.NewGuid();
            TimeStampUtc = DateTime.UtcNow;
            TimeStampLocal = DateTime.Now;
        }
    }
}
