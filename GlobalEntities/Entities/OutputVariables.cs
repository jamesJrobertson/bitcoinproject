﻿using System;

namespace GlobalEntities.Entities
{
    public class OutputVariables : BaseProperties
    {
        public double FinalMoney { get; set; }
        public double MostMoney { get; set; }
        public double LeastMoney { get; set; }
        public TradeCounter Trades { get; set; }
        public double PercentLostToTrades { get; set; }
        public long TimeSpan { get; set; }

        public Guid InputVariablesId { get; set; }
        virtual public InputVariables InputVariables { get; set; }

        public OutputVariables()
        {
            Trades = new TradeCounter();
            PercentLostToTrades = 0;
        }
    }
    
}

