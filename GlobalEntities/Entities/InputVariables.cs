﻿using System.Reflection;
using GlobalEntities.Enums;

namespace GlobalEntities.Entities
{
    public class InputVariables : BaseProperties
    {
        public int SecondLimit { get; set; }
        public double BuyLimitRule { get; set; }
        public double SellBoughtCurrencyLimit { get; set; }
        public double ShortLimitRule { get; set; }
        public double SellShortCurrencyLimit { get; set; }
        public double USDToTrade { get; set; }
        public Algorithm Method { get; set; }
        
        public override bool Equals(object obj)            
        {
            var inputVariables1 = obj as InputVariables;

            foreach (PropertyInfo prop in typeof(InputVariables).GetProperties())
            {
                if (prop.Name != "Id" && prop.Name != "TimeStampUtc" && prop.Name != "TimeStampLocal")
                {
                    // Use !Equals instead of != because != is the same as !ReferenceEquals and we don't care about reference
                    if (!Equals(prop.GetValue(inputVariables1), prop.GetValue(this)))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return $"SecondLimit: {SecondLimit}\n" +
                   $"BuyLimitRule: {BuyLimitRule}\n" +
                   $"SellBoughtCurrencyLimit: {SellBoughtCurrencyLimit}\n" +
                   $"ShortLimitRule: {ShortLimitRule}\n" +
                   $"SellShortCurrencyLimit: {SellShortCurrencyLimit}\n" +
                   $"USDToTrade: {USDToTrade}\n" +
                   $"Algorithm: {Method}\n";
        }        
    }
}
