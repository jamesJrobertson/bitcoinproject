﻿namespace GlobalEntities.Entities
{
    public class LongDataPoint
    {
        public LongDataPoint()
        {

        }

        public LongDataPoint(long x, long y)
        {
            XValue = x;
            YValue = y;
        }

        public long XValue { get; set; }
        public long YValue { get; set; }

        public override bool Equals(object obj)
        {
            var dataPointObj = obj as LongDataPoint;
            return dataPointObj.XValue == XValue && dataPointObj.YValue == YValue;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
