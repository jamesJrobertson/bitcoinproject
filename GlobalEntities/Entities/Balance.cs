﻿using GlobalEntities.Enums;

namespace GlobalEntities.Entities
{
    public class Balance : BaseProperties
    {
        public Currencies CurrencyType { get; set; }
        public WalletType WalletType { get; set; }
        public double AvailableAmount { get; set; }
        public double TotalAmount { get; set; }

        public override string ToString()
        {
            return $"{CurrencyType}: Total - {TotalAmount}, Available - {AvailableAmount}";
        }
    }
}
