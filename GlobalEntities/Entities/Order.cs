﻿using GlobalEntities.Enums;
using System;

namespace GlobalEntities.Entities
{
    public class Order : BaseProperties
    {
        public long OrderId { get; set; } // An order object containing the order’s ID
        public string Symbol { get; set; } // The symbol name the order belongs to
        public string Exchange { get; set; } // "bitfinex”
        public float Price { get; set; } // The price the order was issued at (can be null for market orders)
        public float AvgExecutionPrice { get; set; } // The average price at which this order as been executed so far. 0 if the order has not been executed at all
        public TradeType Side { get; set; } // // Either “buy” or “sell
        public OrderType OrderType { get; set; } // Either “market” / “limit” / “stop” / “trailing-stop”
        public DateTime Timestamp { get; set; } // The timestamp the order was submitted
        public bool IsLive { get; set; } // Could the order still be filled? 
        public bool IsCancelled { get; set; } // Has the order been cancelled?
        public bool IsHidden { get; set; } // Is the order hidden?
        public long OcoOrder { get; set; } // If the order is an OCO order, the ID of the linked order. Otherwise, null
        public bool WasForced { get; set; } // For margin only true if it was forced by the system 
        public float ExecutedAmount { get; set; } // For margin only true if it was forced by the system
        public float RemainingAmount { get; set; } // How much is still remaining to be submitted? 
        public float OriginalAmount { get; set; } // What was the order originally submitted for?

        //public override string ToString()
        //{
        //    return $"symbol: {symbol}\n" +
        //           $"amount: {amount}\n" +
        //            $"price: {price}\n" +
        //            $"side: {side}\n" +
        //            $"type: {type}\n" +
        //            $"exchange: {exchange}\n" +
        //            $"is_hidden: {is_hidden}\n" +
        //            $"is_postonly: {is_postonly}\n" +
        //            $"user_all_available: {user_all_available}\n" +
        //            $"ocoorder: {ocoorder}\n" +
        //            $"buy_price_oco: {buy_price_oco}\n" +
        //            $"sell_price_oco: {sell_price_oco}\n";
        //}
    }
}
