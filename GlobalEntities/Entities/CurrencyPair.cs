﻿using GlobalEntities.Enums;

namespace GlobalEntities.Entities
{
    public class CurrencyPair : BaseProperties
    {
        public Currencies FirstCurrency { get; set; }
        public Currencies SecondCurrency { get; set; }

        public override string ToString()
        {
            return $"{FirstCurrency}{SecondCurrency}";
        }
    }
}
