﻿namespace GlobalEntities.Entities
{
    public class TradeCounter
    {
        public int Buys       { get; set; }
        public int Shorts     { get; set; }
        public int BuysSold   { get; set; }
        public int ShortsSold { get; set; }

        public TradeCounter()
        {
            Buys       = 0;
            Shorts     = 0;
            BuysSold   = 0;
            ShortsSold = 0;
        }

        public int TotalTrades()
        {
            return Buys + Shorts + BuysSold + ShortsSold;
        }
    }
}
