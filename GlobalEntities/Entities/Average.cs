﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace GlobalEntities.Entities
{
    public class Average : BaseProperties
    {
        public double AveragePrice { get; set; }
        public bool IsBuy { get; set; }
        public double TotalAmount { get; set; }
        public double TotalCount { get; set; }

        public virtual ICollection<Trade> Trades { get; set; } = new Collection<Trade>();

        public override string ToString()
        {
            // Catch case where we read in the database and the average has no trades
            // crashing the code, I don't know why, commenting it out
            // System.NullReferenceException: 'Object reference not set to an instance of an object.'
            // GlobalEntities.Entities.Trade.CurrencyPair.get returned null
            // Occurs when retrieving averages table in database, row where trade volume = 0.18 has some trade for
            // some reason
            return //$"{Trades.First().CurrencyPair.ToString()}\n" +
            $"Buy: {IsBuy}\n" +
            $"{nameof(AveragePrice)}: {AveragePrice}\n" +
            $"{nameof(TotalAmount)}: {TotalAmount}\n" +
            $"{nameof(TotalCount)}: {TotalCount}\n" +
            $"{nameof(TimeStampLocal)}: {TimeStampLocal}";
        }
    }
}
