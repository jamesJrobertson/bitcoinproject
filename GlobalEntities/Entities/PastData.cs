﻿namespace GlobalEntities.Entities
{
    public class PastData : BaseProperties
    {        
        public int TimeStampUnix { get; set; }
        public double TotalAmount { get; set; }
        public double TotalUSD { get; set; }        
        public double WeightedUSD { get; set; }                        
        public CurrencyPair CurrencyPair { get; set; }

        public override string ToString()
        {
            return $"{nameof(TimeStampUnix)}: {TimeStampUnix}/n" +
                $"{nameof(TimeStampUtc)}: {TimeStampUtc}/n" +
                $"{nameof(TimeStampLocal)}: {TimeStampLocal}/n" +
                $"{nameof(CurrencyPair)}: {CurrencyPair.ToString()}/n" +
                $"{nameof(TotalAmount)}: {TotalAmount}/n" +
                $"{nameof(TotalUSD)}: {TotalUSD}/n" +
                $"{nameof(WeightedUSD)}: {WeightedUSD}";                
        }
    }
}
