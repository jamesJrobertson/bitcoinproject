﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using TraderController.IndicationsAndRules;
using AnalysisController.DataAnalysis;
using AnalysisController.StatsCalculator;
using GlobalEntities.Enums;
using GlobalEntities.Entities;
using System.Collections.Generic;
using System.Linq;
using OxyPlot;

namespace TestController
{
    [TestClass]
    public static class TestPastDataAnalyzer
    {
        public static void RunTests()
        {
            TestSlopeBuyRules_ShouldBuyAndShort_WhenGivenLargeSlopes();
            TestSlopeSellRules_ShouldSell_WhenGivenSmallSlopes();
            TestLimitPastData_ShouldLimitData_WhenRangeGiven();
            TestTestPastData_ShouldBuyAndShort_WhenGivenLargeSlopes();
        }

        [TestMethod]
        public static void TestSlopeBuyRules_ShouldBuyAndShort_WhenGivenLargeSlopes()
        {
            double buyRule = 0.5;
            double shortRule = -0.5;

            Assert.AreEqual(BitcoinStates.buying, Rules.slopeBuyRules(1, buyRule, shortRule));
            Assert.AreEqual(BitcoinStates.shorting, Rules.slopeBuyRules(-1, buyRule, shortRule));
            Assert.AreEqual(BitcoinStates.pending, Rules.slopeBuyRules(0, buyRule, shortRule));
        }

        [TestMethod]
        public static void TestSlopeSellRules_ShouldSell_WhenGivenSmallSlopes()
        {
            double sellBoughtRule = 0.5;
            double sellShortRule = -0.5;

            Assert.AreEqual(BitcoinStates.sellingBought, Rules.slopeSellRules(0, sellBoughtRule, sellShortRule, BitcoinStates.boughtBitcoin));
            Assert.AreEqual(BitcoinStates.boughtBitcoin, Rules.slopeSellRules(1, sellBoughtRule, sellShortRule, BitcoinStates.boughtBitcoin));
            Assert.AreEqual(BitcoinStates.sellingShort, Rules.slopeSellRules(0, sellBoughtRule, sellShortRule, BitcoinStates.shortedBitcoin));
            Assert.AreEqual(BitcoinStates.shortedBitcoin, Rules.slopeSellRules(-1, sellBoughtRule, sellShortRule, BitcoinStates.shortedBitcoin));
        }
        [TestMethod]
        public static void TestLimitPastData_ShouldLimitData_WhenRangeGiven()
        {
            var dataPoints = new List<LongDataPoint>
            {
                 new LongDataPoint (1, 1),
                 new LongDataPoint (2, 2),
                 new LongDataPoint (3, 3),
                 new LongDataPoint (4, 2),
                 new LongDataPoint (5, 1),
                 new LongDataPoint (6, 2)
            };

            // Will return the current index (1) and the previous 1 since that's all we asked for. So use GetRange(0,2);
            var newDataPoints = LimitPastDataClass.LimitPastData(dataPoints, 1, 1);            
            Assert.IsTrue(newDataPoints.SequenceEqual(dataPoints.GetRange(0,2)));
        }

        [TestMethod]
        // Test order, buy, do nothing, sell, short, do nothing, sell short
        public static void TestTestPastData_ShouldBuyAndShort_WhenGivenLargeSlopes()
        {
            var dataPoints = new List<LongDataPoint>
            {
                 new LongDataPoint (0, 1),
                 new LongDataPoint (1, 2),
                 new LongDataPoint (2, 3),
                 new LongDataPoint (3, 3),
                 new LongDataPoint (4, 2),
                 new LongDataPoint (5, 1),
                 new LongDataPoint (6, 1)
            };

            int    secondLimit             = 1;
            double buyLimitRule            = 0.5;
            double sellBoughtCurrencyLimit = 0.5;
            double shortLimitRule          = -0.5;
            double sellShortCurrencyLimit  = -0.5;
            double USDToTrade              = 1;

            var totalUSD = new List<DataPoint>();
            // Loop through the list 1 value at a time to see how it acts at all times
            for (var i = 1; i <= dataPoints.Count; i++)
            {
                totalUSD = PastDataAnalyzer.TestPastData(dataPoints.GetRange(0, i), secondLimit, buyLimitRule, sellBoughtCurrencyLimit,
                            shortLimitRule, sellShortCurrencyLimit, USDToTrade);

                switch (i)
                {
                    // No case 1 because there is only 1 point of data so no totalUSD is returned
                    case 2:
                        Assert.AreEqual(-0.002, totalUSD[totalUSD.Count - 1].Y);
                        break;
                    case 3:
                        Assert.AreEqual(-0.002, totalUSD[totalUSD.Count - 1].Y);
                        break;
                    case 4:
                        Assert.AreEqual(0.496, totalUSD[totalUSD.Count - 1].Y);
                        break;
                    case 5:
                        Assert.AreEqual(0.494, totalUSD[totalUSD.Count - 1].Y);
                        break;
                    case 6:
                        Assert.AreEqual(0.494, totalUSD[totalUSD.Count - 1].Y);
                        break;
                    case 7:
                        Assert.AreEqual(0.992, totalUSD[totalUSD.Count - 1].Y);
                        break;
                }
            }
        }
    } 
}
