﻿using GlobalEntities.Entities;
using GlobalEntities.Enums;
using NUnit.Framework;
using System;

namespace TestController.Database.GlobalEntities.Entities
{
    [TestFixture]
    class InputVariablesFixture
    {
        [Test]
        public void SameInputs_ShouldReturnTrueOrFalse_WhenGivenInputVariablesObject()
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            InputVariables inputVariables1, inputVariables2 = new InputVariables();
            inputVariables1 = new InputVariables()
            {
                SecondLimit = 86400,
                BuyLimitRule = 0.015,
                SellBoughtCurrencyLimit = 0.05,
                ShortLimitRule = -0.015,
                SellShortCurrencyLimit = -0.05,
                USDToTrade = 100,
                Method = Algorithm.poly1 // first order polynomial (slope)        
            };
            // Couldn't figure out how to clone so just declared another input var
            inputVariables2 = new InputVariables()
            {
                SecondLimit = 86400,
                BuyLimitRule = 0.015,
                SellBoughtCurrencyLimit = 0.05,
                ShortLimitRule = -0.015,
                SellShortCurrencyLimit = -0.05,
                USDToTrade = 100,
                Method = Algorithm.poly1 // first order polynomial (slope)        
            };

            // SameInputs has if statement that should return true even if the times and Id are different
            inputVariables1.TimeStampUtc   = new DateTime(1,1,1);
            inputVariables1.TimeStampLocal = new DateTime(1, 1, 1);
            inputVariables1.Id = new Guid();

            
            Assert.True(inputVariables2.Equals(inputVariables1));
            inputVariables2.SecondLimit = 1;
            Assert.False(inputVariables2.Equals(inputVariables1));

            Assert.Less(watch.ElapsedMilliseconds, 10); // run seperately takes 7 seconds
        }
    }
}