﻿using System.Collections.Generic;
using AnalysisController.DataAnalysis;
using NUnit.Framework;
using GlobalEntities.Entities;

namespace TestController.Controllers.AnalysisController.DataAnalysis
{
    [TestFixture]
    public class PastDataAnalyzerFixture
    {
        private List<LongDataPoint> _dataPoints;
        private InputVariables _inputVariables;

        [SetUp]
        public void Setup()
        {
            _dataPoints = new List<LongDataPoint>
            {
                 new LongDataPoint (0,   0),
                 new LongDataPoint (60,  60),  //buy
                 new LongDataPoint (120, 120), // do nothing 
                 new LongDataPoint (180, 120), // sell bought
                 new LongDataPoint (240, 60),  // short
                 new LongDataPoint (300, 0),   // do nothing
                 new LongDataPoint (360, 0)    // sell
            };
                
            _inputVariables = new InputVariables()
            {
                SecondLimit = 60,
                BuyLimitRule = 0.5,
                SellBoughtCurrencyLimit = 0.5,
                ShortLimitRule = -0.5,
                SellShortCurrencyLimit = -0.5,
                USDToTrade = 1
            };
        }

        [Test]        
        public void AnalyzePastData_ShouldReturnMoneyAndTrades_WhenGivenDataSet()
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var outputVariables = new OutputVariables();
            var money = new List<double>();
            var slopes = new List<double>();
            double _limitDataPercent = 0;
            // Loop through the list 1 value at a time
            for (var i = 1; i <= _dataPoints.Count; i++)
            {
                (outputVariables, money, slopes) = new PastDataAnalyzer().AnalyzePastData(_dataPoints.GetRange(0, i), _inputVariables, _limitDataPercent, true);

                switch (i)
                {
                    // No case 1 because there is only 1 point of data so no outputVariables.CurrencyAmount is returned                                        
                    //buy
                    case 2:
                        Assert.AreEqual(-0.002, outputVariables.FinalMoney);
                        Assert.AreEqual(1, outputVariables.Trades.Buys);
                        Assert.AreEqual(0, outputVariables.Trades.Shorts);
                        Assert.AreEqual(0, outputVariables.Trades.BuysSold);
                        Assert.AreEqual(0, outputVariables.Trades.ShortsSold);
                        Assert.AreEqual(0.002, outputVariables.PercentLostToTrades);
                        break;
                    // do nothing 
                    case 3:
                        Assert.AreEqual(-0.002, outputVariables.FinalMoney);
                        Assert.AreEqual(1, outputVariables.Trades.Buys);                        
                        Assert.AreEqual(0, outputVariables.Trades.Shorts);
                        Assert.AreEqual(0, outputVariables.Trades.BuysSold);
                        Assert.AreEqual(0, outputVariables.Trades.ShortsSold);
                        Assert.AreEqual(0.002, outputVariables.PercentLostToTrades);
                        break;
                    // sell bought
                    case 4:
                        Assert.AreEqual(0.996, outputVariables.FinalMoney);
                        Assert.AreEqual(1, outputVariables.Trades.Buys);
                        Assert.AreEqual(0, outputVariables.Trades.Shorts);
                        Assert.AreEqual(1, outputVariables.Trades.BuysSold);
                        Assert.AreEqual(0, outputVariables.Trades.ShortsSold);
                        Assert.AreEqual(0.004, outputVariables.PercentLostToTrades);
                        break;
                    // do nothing
                    //case 5:
                    //    Assert.AreEqual(0.996, outputVariables.FinalMoney);
                    //    Assert.AreEqual(1, outputVariables.Trades.Buys);
                    //    Assert.AreEqual(0, outputVariables.Trades.Shorts);
                    //    Assert.AreEqual(1, outputVariables.Trades.BuysSold);
                    //    Assert.AreEqual(0, outputVariables.Trades.ShortsSold);
                    //    Assert.AreEqual(0.004, outputVariables.PercentLostToTrades);
                    //    break;
                    //    // do nothing
                    //case 6:
                    //    Assert.AreEqual(0.996, outputVariables.FinalMoney);
                    //    Assert.AreEqual(1, outputVariables.Trades.Buys);
                    //    Assert.AreEqual(0, outputVariables.Trades.Shorts);
                    //    Assert.AreEqual(1, outputVariables.Trades.BuysSold);
                    //    Assert.AreEqual(0, outputVariables.Trades.ShortsSold);
                    //    Assert.AreEqual(0.004, outputVariables.PercentLostToTrades);
                    //    break;
                    //    // do nothing
                    //case 7:
                    //    Assert.AreEqual(0.996, outputVariables.FinalMoney);
                    //    Assert.AreEqual(1, outputVariables.Trades.Buys);
                    //    Assert.AreEqual(0, outputVariables.Trades.Shorts);
                    //    Assert.AreEqual(1, outputVariables.Trades.BuysSold);
                    //    Assert.AreEqual(0, outputVariables.Trades.ShortsSold);
                    //    Assert.AreEqual(0.004, outputVariables.PercentLostToTrades);
                    //    break;
                    // old code when we thought we could short

                    // short
                    case 5:
                        Assert.AreEqual(0.994, outputVariables.FinalMoney);
                        Assert.AreEqual(1, outputVariables.Trades.Buys);
                        Assert.AreEqual(1, outputVariables.Trades.Shorts);
                        Assert.AreEqual(1, outputVariables.Trades.BuysSold);
                        Assert.AreEqual(0, outputVariables.Trades.ShortsSold);
                        Assert.AreEqual(0.006, outputVariables.PercentLostToTrades);
                        break;
                        //do nothing
                        case 6:
                        Assert.AreEqual(0.994, outputVariables.FinalMoney);
                        Assert.AreEqual(1, outputVariables.Trades.Buys);
                        Assert.AreEqual(1, outputVariables.Trades.Shorts);
                        Assert.AreEqual(1, outputVariables.Trades.BuysSold);
                        Assert.AreEqual(0, outputVariables.Trades.ShortsSold);
                        Assert.AreEqual(0.006, outputVariables.PercentLostToTrades);
                        break;
                        // sell
                        case 7:
                        Assert.AreEqual(1.992, outputVariables.FinalMoney);
                        Assert.AreEqual(1, outputVariables.Trades.Buys);
                        Assert.AreEqual(1, outputVariables.Trades.Shorts);
                        Assert.AreEqual(1, outputVariables.Trades.BuysSold);
                        Assert.AreEqual(1, outputVariables.Trades.ShortsSold);
                        Assert.AreEqual(0.008, outputVariables.PercentLostToTrades);
                        break;
                }
            }
            //Assert.AreEqual(2, outputVariables.Trades.TotalTrades());
            //Assert.AreEqual(360, outputVariables.TimeSpan);
            //Assert.AreEqual(-0.002, outputVariables.LeastMoney);
            //Assert.AreEqual(0.996, outputVariables.MostMoney);
            
            // old code when we thought we could short
            Assert.AreEqual(4, outputVariables.Trades.TotalTrades());
            Assert.AreEqual(360, outputVariables.TimeSpan);
            Assert.AreEqual(-0.002, outputVariables.LeastMoney);
            Assert.AreEqual(1.992, outputVariables.MostMoney);

            watch.Stop();
            Assert.Less(watch.ElapsedMilliseconds, 80);
        }
    }
}
