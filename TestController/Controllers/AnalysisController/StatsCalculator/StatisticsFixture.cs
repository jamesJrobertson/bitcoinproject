﻿using AnalysisController.StatsCalculator;
using BitfinexApi;
using GlobalEntities.Entities;
using NUnit.Framework;
using System.Collections.Generic;

namespace TestController.Controllers.AnalysisController.StatsCalculator
{
    [TestFixture]
    class StatisticsFixture
    {
        [Test]                
        public void FixAveragesIfMissingData_ShouldEditAverages_WhenGivenNoData()
        {            
            var watch = System.Diagnostics.Stopwatch.StartNew();
            
            var average = new Statistics().CreateAverages(new TradeResponse());            
            List<Average> pastMinuteAverage = new List<Average>(new Average[2]);            
            average[0] = new Average()
            {
                AveragePrice = 0,
                IsBuy = true,
                TotalAmount = 0,
                TotalCount = 0
            };

            pastMinuteAverage[0] = new Average()
            {
                AveragePrice = 1,
                IsBuy = true,
                TotalAmount = 1,
                TotalCount = 1
            };
            
            (var newAverages, var returnPastMinuteAverages) = new Statistics().fixAveragesIfMissingData(average, pastMinuteAverage);
            Assert.AreEqual(pastMinuteAverage[0].AveragePrice, newAverages[0].AveragePrice);

            Assert.Less(watch.ElapsedMilliseconds, 20);
        }
    }
}
