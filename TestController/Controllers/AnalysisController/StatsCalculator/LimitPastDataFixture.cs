﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using GlobalEntities.Entities;
using AnalysisController.StatsCalculator;

namespace TestController.Controllers.AnalysisController.StatsCalculator
{
    [TestFixture]
    public class LimitDataFixture
    {
        [Test]
        public void LimitData_ShouldLimitData_WhenRangeGiven()
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var dataPoints = new List<LongDataPoint>
            {
                 new LongDataPoint (60,  1),
                 new LongDataPoint (120, 2),
                 new LongDataPoint (180, 3),
                 new LongDataPoint (240, 2),
                 new LongDataPoint (300, 1),
                 new LongDataPoint (360, 2)
            };

            // Will return the current index(1) and the previous point since that's all we asked for. So use GetRange(0,2);
            (var newDataPoints, var haveEnoughData) = new LimitDataClass().LimitData(dataPoints, 1, 60, 0.8);
            Assert.IsTrue(newDataPoints.SequenceEqual(dataPoints.GetRange(0, 2)));
            Assert.IsTrue(haveEnoughData);

            // Check if only 1 data point is returned that haveEnoughData is false
            (newDataPoints, haveEnoughData) = new LimitDataClass().LimitData(dataPoints, 0, 60, 0.8);
            Assert.IsFalse(haveEnoughData);

            // Check if we average over past 2 seconds, and there is only 2 point, that it returns false
            // (need 2.4 [3 * 0.8] points to average over 2 seconds)
            (newDataPoints, haveEnoughData) = new LimitDataClass().LimitData(dataPoints, 1, 120, 0.8);
            Assert.IsFalse(haveEnoughData);

            // (need  ((300 / 60 + 1) * 0.8] = 4.8 points to average over 5 seconds)
            // should have 5 points, so haveEnoughData will be true
            (newDataPoints, haveEnoughData) = new LimitDataClass().LimitData(dataPoints, 4, 300, 0.8);
            Assert.IsTrue(haveEnoughData);

            Assert.Less(watch.ElapsedMilliseconds, 7);
        }
    }
}
