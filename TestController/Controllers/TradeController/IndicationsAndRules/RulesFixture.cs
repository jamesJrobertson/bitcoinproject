﻿using NUnit.Framework;
using TraderController.IndicationsAndRules;
using GlobalEntities.Enums;

namespace TestController.Controllers.TradeController.IndicationsAndRules
{
    [TestFixture]
    public class RulesFixture
    {
        Rules _rules = new Rules();
        [Test]
        //public void SlopeBuyRules_ShouldBuyOrShortOrDoNothing_WhenGivenSlope()
        public void SlopeBuyRules_ShouldBuyOrDoNothing_WhenGivenSlope()
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            double buyRule = 0.5;            

            //Assert.AreEqual(BitcoinStates.buying, _rules.SlopeBuyRules(1, buyRule));
            //Assert.AreEqual(BitcoinStates.pending, _rules.SlopeBuyRules(0, buyRule));            

            // old code when we thought we could short
            double shortRule = -0.5;
            Assert.AreEqual(BitcoinStates.pending,  _rules.SlopeBuyRules(1, 1, buyRule, shortRule));
            Assert.AreEqual(BitcoinStates.buying,   _rules.SlopeBuyRules(0, 1, buyRule, shortRule));
            Assert.AreEqual(BitcoinStates.shorting, _rules.SlopeBuyRules(0, -1, buyRule, shortRule));
            Assert.AreEqual(BitcoinStates.pending,  _rules.SlopeBuyRules(-1, -1, buyRule, shortRule));

            Assert.Less(watch.ElapsedMilliseconds, 33);
        }

        [Test]
        public void SlopeSellRules_ShouldSellBoughtOrDoNothing_WhenGivenSlope()
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            double sellBoughtRule = 0.5;

            //Assert.AreEqual(BitcoinStates.sellingBought, _rules.SlopeSellRules(0, sellBoughtRule, BitcoinStates.boughtBitcoin));
            //Assert.AreEqual(BitcoinStates.boughtBitcoin, _rules.SlopeSellRules(1, sellBoughtRule, BitcoinStates.boughtBitcoin));
            //// Below case should not happen, should throw error then, or whatever
            //Assert.AreEqual(BitcoinStates.pending, _rules.SlopeSellRules(1, sellBoughtRule, BitcoinStates.pending));
            
            // old code when we thought we could short
            double sellShortRule = -0.5;
            Assert.AreEqual(BitcoinStates.sellingBought, _rules.SlopeSellRules(1, 0, sellBoughtRule, sellShortRule, BitcoinStates.boughtBitcoin));
            Assert.AreEqual(BitcoinStates.boughtBitcoin, _rules.SlopeSellRules(1, 1, sellBoughtRule, sellShortRule, BitcoinStates.boughtBitcoin));
            Assert.AreEqual(BitcoinStates.boughtBitcoin, _rules.SlopeSellRules(0, 0, sellBoughtRule, sellShortRule, BitcoinStates.boughtBitcoin));
            Assert.AreEqual(BitcoinStates.boughtBitcoin, _rules.SlopeSellRules(0, 1, sellBoughtRule, sellShortRule, BitcoinStates.boughtBitcoin));

            Assert.AreEqual(BitcoinStates.sellingShort, _rules.SlopeSellRules(-1, 0, sellBoughtRule, sellShortRule, BitcoinStates.shortedBitcoin));
            Assert.AreEqual(BitcoinStates.shortedBitcoin, _rules.SlopeSellRules(0, 0, sellBoughtRule, sellShortRule, BitcoinStates.shortedBitcoin));            
            Assert.AreEqual(BitcoinStates.shortedBitcoin, _rules.SlopeSellRules(0, -1, sellBoughtRule, sellShortRule, BitcoinStates.shortedBitcoin));
            Assert.AreEqual(BitcoinStates.shortedBitcoin, _rules.SlopeSellRules(-1,-1, sellBoughtRule, sellShortRule, BitcoinStates.shortedBitcoin));

            Assert.Less(watch.ElapsedMilliseconds, 26);
        }

        //[Test]
        //public void ConcavitySellRules_ShouldSellBoughtOrDoNthing_WhenGivenConcavity()
        //{
        //    var watch = System.Diagnostics.Stopwatch.StartNew();
        //    Assert.AreEqual(BitcoinStates.boughtBitcoin, _rules.ConcavitySellRules(-1, -1, BitcoinStates.boughtBitcoin));
        //    Assert.AreEqual(BitcoinStates.sellingBought, _rules.ConcavitySellRules(-1,  1, BitcoinStates.boughtBitcoin));
        //    Assert.AreEqual(BitcoinStates.boughtBitcoin, _rules.ConcavitySellRules( 1, -1, BitcoinStates.boughtBitcoin));
        //    Assert.AreEqual(BitcoinStates.boughtBitcoin, _rules.ConcavitySellRules( 1,  1, BitcoinStates.boughtBitcoin));


        //    Assert.AreEqual(BitcoinStates.sellingShort, _rules.ConcavitySellRules(-1, 1, BitcoinStates.shortedBitcoin));
        //    Assert.AreEqual(BitcoinStates.shortedBitcoin, _rules.ConcavitySellRules(-1, 1, BitcoinStates.shortedBitcoin));
        //    Assert.Less(watch.ElapsedMilliseconds, 1);
        //}
    }
}
