﻿using System.Collections.Generic;
using GlobalEntities.Entities;
using Context.BitcoinDatabase;
using Queries.OutputVariablesEntity.OutputVariablesQuery;

namespace Queries.OutputVariablesEntity
{
    public class OutputVariablesQueries
    {
        BitcoinContext _context;

        public OutputVariablesQueries(BitcoinContext context)
        {
            _context = context;
        }

        public IEnumerable<OutputVariables> GetOutputVariablesQuery()
        {
            return new GetOutputVariablesOrderedByFinalMoneyAscendingQuery(_context.OutputVariables).Execute();
        }
    }
}
