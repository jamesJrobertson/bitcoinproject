﻿using System.Collections.Generic;
using System.Data.Entity;
using GlobalEntities.Entities;
using System.Linq;

namespace Queries.OutputVariablesEntity.OutputVariablesQuery
{
    /// <summary>    
    /// Gets all the combinations of output variables we have tried from the Output variables table
    /// </summary>
    public class GetOutputVariablesOrderedByFinalMoneyAscendingQuery
    {
        private DbSet<OutputVariables> _OutputVariables;

        public GetOutputVariablesOrderedByFinalMoneyAscendingQuery(DbSet<OutputVariables> OutputVariables)
        {
            _OutputVariables = OutputVariables;
        }

        public IEnumerable<OutputVariables> Execute()
        {
            return _OutputVariables
                .OrderBy(o => o.FinalMoney);
        }
    }
}
