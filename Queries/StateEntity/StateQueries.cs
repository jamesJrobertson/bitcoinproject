﻿using Context.BitcoinDatabase;
using Queries.StateEntity.StateQuery;
using GlobalEntities.Entities;
using GlobalEntities.Enums;

namespace Queries.StateEntity
{
    public class StateQueries
    {
        BitcoinContext _context;

        public StateQueries(BitcoinContext context)
        {
            _context = context;
        }

        public BitcoinStates GetState()
        {
            return new GetState(_context.AlgorithmState).Execute();
        }
    }   
}
