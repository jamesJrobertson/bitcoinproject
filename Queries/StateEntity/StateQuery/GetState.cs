﻿using System.Data.Entity;
using System.Linq;
using GlobalEntities.Entities;
using GlobalEntities.Enums;

namespace Queries.StateEntity.StateQuery
{
    /// <summary>
    /// Gets the state of the algorithm (pending, buying, bought, selling)
    /// </summary>
    public class GetState
    {
        private DbSet<AlgorithmState> _state;

        public GetState(DbSet<AlgorithmState> state)
        {
            _state = state;
        }
        
        public BitcoinStates Execute()
        {
            // Check if table is empty
            // If so, return pending state
            if (!_state.Any())
            {
                return BitcoinStates.pending;
            }
            else
            {
                return _state.First().BitcoinState;
            }
            
        }
    }
}
