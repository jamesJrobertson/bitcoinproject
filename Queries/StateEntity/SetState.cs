﻿using Context.BitcoinDatabase;
using GlobalEntities.Entities;
using GlobalEntities.Enums;

namespace Queries.StateEntity
{
    public class SetState
    {
        /// <summary>
        /// Set the state of the algorithm (pending, buying, bought, selling)
        /// </summary>
        public void Execute(BitcoinContext context, BitcoinStates state)
        {
            var algorithmState = new AlgorithmState()
            {
                BitcoinState = state
            };
                
            context.Database.ExecuteSqlCommand("TRUNCATE TABLE [AlgorithmStates]");                        
            context.AlgorithmState.Add(algorithmState);
            context.SaveChanges();
        }
    }
}