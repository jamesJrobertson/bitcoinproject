﻿using System.Collections.Generic;
using System.Data.Entity;
using GlobalEntities.Entities;
using System.Linq;

namespace Queries.InputVariablesEntity.InputVariablesQuery
{
    /// <summary>    
    /// Gets all the combinations of input variables we have tried from the input variables table
    /// </summary>
    public class GetInputVariablesOrderedBySecondLimitAscendingQuery
    {
        private DbSet<InputVariables> _inputVariables;
        
        public GetInputVariablesOrderedBySecondLimitAscendingQuery(DbSet<InputVariables> inputVariables)       
        {            
            _inputVariables = inputVariables;
        }

        public IEnumerable<InputVariables> Execute()
        {            
            return _inputVariables                
                .OrderBy(i => i.SecondLimit); // order by secondLimit in case table is not sorted
        }
    }
}
