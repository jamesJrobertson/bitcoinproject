﻿using System.Collections.Generic;
using System.Data.Entity;
using GlobalEntities.Entities;
using System.Linq;
using System;
using HelpersController.Time;

namespace Queries.BitcoinAveragesEntity.BitcoinAveragesQuery
{
    /// <summary>
    /// Gets all of the average bitcoin price sorted by utc time stamp from the averages table
    /// </summary>
    public class GetBitcoinAveragesOrderByTimeStampUtcQuery
    {
        private DbSet<Average> _bitcoinAverages;

        public GetBitcoinAveragesOrderByTimeStampUtcQuery(DbSet<Average> bitcoinAverages)
        {
            _bitcoinAverages = bitcoinAverages;
        }                 

        public IEnumerable<Average> Execute()
        {
            return _bitcoinAverages
                .OrderBy(b => b.TimeStampUtc);
        }
    }
}
