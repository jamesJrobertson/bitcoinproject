﻿using System.Collections.Generic;
using GlobalEntities.Entities;
using Context.BitcoinDatabase;
using Queries.BitcoinAveragesEntity.BitcoinAveragesQuery;

namespace Queries.BitcoinAveragesEntity
{
    public class BitcoinAveragesQueries
    {
        BitcoinContext _context;

        public BitcoinAveragesQueries(BitcoinContext context)
        {
            _context = context;
        }

        public IEnumerable<Average> GetBitcoinAveragesOrderByTimeStampUtc()
        {
            return new GetBitcoinAveragesOrderByTimeStampUtcQuery(_context.Averages).Execute();
        }
    }
}
