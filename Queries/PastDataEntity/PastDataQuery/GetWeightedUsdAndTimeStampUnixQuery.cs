﻿using System.Collections.Generic;
using System.Data.Entity;
using GlobalEntities.Entities;
using System.Linq;

namespace Queries.PastDataEntity.PastDataQuery
{
    /// <summary>
    /// Gets all of the average bitcoin price sorted by unix time stamp from the averages table
    /// </summary>
    public class GetWeightedUsdAndTimeStampUnixQuery
    {
        private DbSet<PastData> _pastData;

        public GetWeightedUsdAndTimeStampUnixQuery(DbSet<PastData> pastData)
        {
            _pastData = pastData;
        }

        public IEnumerable<LongDataPoint> Execute()
        {
            return _pastData
                .OrderBy(p => p.TimeStampUnix)
                .Select(p => new LongDataPoint
                            {
                                XValue = p.TimeStampUnix,
                                YValue = (long)p.WeightedUSD
                            }
                        );
        }
    }
}
