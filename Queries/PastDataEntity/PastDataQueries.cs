﻿using System.Collections.Generic;
using GlobalEntities.Entities;
using Context.BitcoinDatabase;
using Queries.PastDataEntity.PastDataQuery;

namespace Queries.PastDataEntity
{
    public class PastDataQueries
    {
        BitcoinContext _context;

        public PastDataQueries(BitcoinContext context)
        {
            _context = context;
        }

        public IEnumerable<LongDataPoint> GetWeightedUsdAndTimeStampUnix()
        {
            return new GetWeightedUsdAndTimeStampUnixQuery(_context.PastData).Execute();
        }
    }
}
