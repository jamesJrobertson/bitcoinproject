﻿using System;
using System.IO;

namespace HelpersController.Logger
{
    /// <summary>
    /// Logs exceptions to a text file for later analysis    
    /// </summary>
    public class LoggerManager
    {
        Exception _ex;
        
        public LoggerManager(Exception ex)
        {
            _ex = ex;
        }

        /// <summary>
        /// Logs exception to text file log.txt in root direction (I think)
        /// taken from here https://stackoverflow.com/questions/21307789/how-to-save-exception-in-txt-file
        /// </summary>        
        public void Logger()
        {            
            using (StreamWriter writer = new StreamWriter(@"..\..\..\log.txt", true))
            {
                // TODO, find current path
                writer.WriteLine("Message :" + _ex.Message + Environment.NewLine + "StackTrace :" + _ex.StackTrace +
                "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
        }
    }
}
