﻿using System;

namespace HelpersController.Time
    // Note: Net 4.6 now has support for these conversions, don't need custom fuctions
    // https://stackoverflow.com/questions/249760/how-can-i-convert-a-unix-timestamp-to-datetime-and-vice-versa
{
    /// <summary>
    /// Helper class for converting time to bitfinex standards.
    /// </summary>
    public static class TimeConversions
    {
        // Unix start date
        private static DateTime epoch = new DateTime(1970, 1, 1);

        // Unix utc now
        public static double Nonce => (DateTime.UtcNow - epoch).TotalSeconds;

        /// <summary>
        /// Converts the given seconds from unix to a formatted <see cref="DateTime"/> in utc.
        /// </summary>
        /// <param name="unixSeconds"> The date in unix seconds. </param>
        /// <returns> Utc date time. </returns>
        public static DateTime UnixSecondsToUtcDateTime(double unixSeconds) => epoch.AddSeconds(unixSeconds);

        /// <summary>
        /// Converts the given seconds from unix to a formatted <see cref="DateTime"/> in the local timezone.
        /// </summary>
        /// <param name="unixSeconds"> The date in unix seconds. </param>
        /// <returns> Localized date time. </returns>
        public static DateTime UnixSecondsToLocalDateTime(double unixSeconds) => UnixSecondsToUtcDateTime(unixSeconds).ToLocalTime();

        /// <summary>
        /// Converts the given datetime to unix time.
        /// </summary>
        /// <param name="datetime"> The date in date time format. </param>
        /// <returns> Unix time. </returns>        
        public static double UtcDateTimeToUnixSeconds(DateTime datetime) => (datetime - epoch).TotalSeconds;
    }
}
